// IMusicPlayer.aidl
package com.hedgehog.mousai;

import java.util.List;

interface IMusicPlayer {

    /**
    * Start the playback of the current {@link MusicFile music file}.
    */
    void play();

    /**
    * Pause the playback of the current {@link MusicFile music file}.
    */
    void pause();

    /**
    * Switch the current state of the playback between playing and paused.
    */
    void toggle();

    /**
    * Stop the playback of the current {@link MusicFile music file}.
    */
    void stop();

    /**
    * Play next music file.
    */
    void playNext();

    /**
    * Play previous {@link MusicFile music file}.
    */
    void playPrevious();

    /**
    * Seek to specific position of the current {@link MusicFile music file}.
    */
    void seekTo(int progress);

    /**
    * Check if there is a music file currently playing.
    *
    * @return whether a music file is currently playing or not
    */
    boolean isPlaying();

    /**
    * Shuffle the set playlist of {@link MusicFile music files}
    */
    void shuffle();

    /**
    * Set looping playback of the current {@link MusicFile music file}.
    */
    void setLooping(boolean looping);

    /**
    * Get the progress of the current {@link MusicFile music file}
    */
    long getProgress();


    /**
    * Get the max progress of the current {@link MusicFile music file}
    */
    long getMaxProgress();
}
