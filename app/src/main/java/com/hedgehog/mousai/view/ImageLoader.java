package com.hedgehog.mousai.view;


import android.widget.ImageView;


/**
 * Definition of a class that can load an image associated with a provided item into a {@link ImageView }.
 * 
 * @param <T>
 *            The type of the item the loader with work with to provide an image
 */
public interface ImageLoader<T> {

    /**
     * Load the image associated with the item into the target {@link ImageView}.
     * 
     * @param item
     *            the item for which an image should be provided
     * @param target
     *            the {@link ImageView} where the image should be loaded
     */
    void load(T item, ImageView target);
}
