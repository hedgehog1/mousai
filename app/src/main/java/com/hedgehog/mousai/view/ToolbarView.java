package com.hedgehog.mousai.view;


import android.support.v7.widget.Toolbar;


public interface ToolbarView {

    Toolbar getToolbar();
}
