package com.hedgehog.mousai.view;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.hedgehog.mousai.R;
import com.hedgehog.mousai.event.view.PlayerControlsEvent;
import com.hedgehog.mousai.util.StringUtils;

import de.greenrobot.event.EventBus;


/**
 * A view containing controls for a media player. Contains buttons like "Play/Pause", "Previous", "Next", etc ... This
 * class does not act on any events nor does it have any business logic in it. The events are delegated and it is the
 * client`s responsibility to handle them.
 */
public class PlayerControlsView extends RelativeLayout {

    private static final String TAG = PlayerControlsView.class.getCanonicalName();

    private ImageButton mPlayPause;
    private ImageButton mPlayNext;
    private ImageButton mPlayPrevious;
    private SeekBar     mSeekBar;
    private TextView    mElapsedTimeTextView;
    private TextView    mTotalTimeTextView;
    private EventBus    mEventBus;


    public PlayerControlsView(Context context) {
        super(context);
        init();
    }


    public PlayerControlsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public PlayerControlsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    /**
     * Display the play button.
     */
    public void showPlay() {
        mPlayPause.setImageResource(R.mipmap.ic_player_play);
    }


    /**
     * Display the pause button.
     */
    public void showPause() {
        mPlayPause.setImageResource(R.mipmap.ic_player_pause);
    }


    public void setProgress(int progress, int maxProgress) {
        mSeekBar.setProgress(progress);
        mElapsedTimeTextView.setText(StringUtils.formatTime(progress));

        if (maxProgress != mSeekBar.getMax()) {
            mSeekBar.setMax(maxProgress);
            mTotalTimeTextView.setText(StringUtils.formatTime(maxProgress));
        }
    }


    private void init() {
        mEventBus = EventBus.getDefault();
        initViews();
    }


    private void initViews() {
        initPlayPauseView();
        initPlayNextView();
        initPlayPreviousView();
        initProgressBarView();
        initCurrentTimeView();
        initTotalTimeView();
    }


    private void initPlayPreviousView() {
        mPlayPrevious = new ImageButton(getContext());
        mPlayPrevious.setId(R.id.music_player_play_previous_button_id);
        mPlayPrevious.setImageResource(R.mipmap.ic_player_previous);
        mPlayPrevious.setBackgroundResource(android.R.color.transparent);
        LayoutParams playPreviousLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        playPreviousLP.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        playPreviousLP.addRule(RelativeLayout.CENTER_VERTICAL);
        mPlayPrevious.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mEventBus.post(new PlayerControlsEvent.Previous());
            }
        });
        addView(mPlayPrevious, playPreviousLP);
    }


    private void initPlayNextView() {
        mPlayNext = new ImageButton(getContext());
        mPlayNext.setId(R.id.music_player_play_next_button_id);
        mPlayNext.setImageResource(R.mipmap.ic_player_next);
        mPlayNext.setBackgroundResource(android.R.color.transparent);
        LayoutParams playNextLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        playNextLP.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        playNextLP.addRule(RelativeLayout.CENTER_VERTICAL);
        mPlayNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mEventBus.post(new PlayerControlsEvent.Next());
            }
        });
        addView(mPlayNext, playNextLP);
    }


    private void initPlayPauseView() {
        mPlayPause = new ImageButton(getContext());
        mPlayPause.setId(R.id.music_player_play_pause_button_id);
        mPlayPause.setBackgroundResource(android.R.color.transparent);
        mPlayPause.setImageResource(R.mipmap.ic_player_play);
        LayoutParams playPauseLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        playPauseLP.addRule(RelativeLayout.CENTER_IN_PARENT);
        mPlayPause.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mEventBus.post(new PlayerControlsEvent.PlayPause());
            }
        });
        addView(mPlayPause, playPauseLP);
    }


    private void initProgressBarView() {
        mSeekBar = new SeekBar(getContext());
        mSeekBar.setId(R.id.music_player_seek_bar_id);

        LayoutParams progressBarLP = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        progressBarLP.addRule(RelativeLayout.BELOW, mPlayPause.getId());
        progressBarLP.addRule(RelativeLayout.CENTER_HORIZONTAL);

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mElapsedTimeTextView.setText(StringUtils.formatTime(progress));
            }


            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mEventBus.post(new PlayerControlsEvent.SeekBarTrackingStarted());
            }


            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mEventBus.post(new PlayerControlsEvent.ProgressChanged(seekBar.getProgress()));
            }
        });

        addView(mSeekBar, progressBarLP);
    }


    private void initCurrentTimeView() {
        mElapsedTimeTextView = new TextView(getContext());
        mElapsedTimeTextView.setId(R.id.music_player_elapsed_time_id);
        LayoutParams currentTimeLP = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        currentTimeLP.addRule(RelativeLayout.BELOW, mSeekBar.getId());
        currentTimeLP.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

        mElapsedTimeTextView.setText(getResources().getString(R.string.inital_time));

        addView(mElapsedTimeTextView, currentTimeLP);
    }


    private void initTotalTimeView() {
        mTotalTimeTextView = new TextView(getContext());
        mTotalTimeTextView.setId(R.id.music_player_total_time_id);
        LayoutParams totalTimeLP = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        totalTimeLP.addRule(RelativeLayout.BELOW, mSeekBar.getId());
        totalTimeLP.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

        mTotalTimeTextView.setText(getResources().getString(R.string.inital_time));

        addView(mTotalTimeTextView, totalTimeLP);
    }
}
