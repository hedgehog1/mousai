package com.hedgehog.mousai.view;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.hedgehog.mousai.adapter.MusicListAdapter;
import com.hedgehog.mousai.application.MousaiApplication;
import com.hedgehog.mousai.event.view.MusicListEvent;
import com.hedgehog.mousai.model.music.MusicFile;

import de.greenrobot.event.EventBus;

import javax.inject.Inject;


/**
 * View class for showing a list of music files
 */
public class MusicListView extends LinearLayout {

    @Inject
    EventBus mEventBus;

    private ListView         mListView;
    private MusicListAdapter mMusicListAdapter;


    public MusicListView(Context context) {
        super(context);
        init();
    }


    public MusicListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public MusicListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        MousaiApplication.getViewComponent().inject(this);
        mListView = new ListView(getContext());
        LinearLayout.LayoutParams layoutParams = new LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
          LinearLayout.LayoutParams.WRAP_CONTENT);
        mListView.setPadding(20, 10, 20, 10);
        mListView.setDividerHeight(10);
        mListView.setBackgroundResource(android.R.color.darker_gray);
        addView(mListView, layoutParams);
    }


    /**
     * Set the {@link MusicListAdapter} to be used.
     */
    public void setAdapter(MusicListAdapter adapter) {
        mMusicListAdapter = adapter;
        mListView.setAdapter(mMusicListAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MusicFile selectedMusicFile = mMusicListAdapter.getItem(position);
                MusicListEvent.MusicFileSelected musicFileSelectedEvent = new MusicListEvent.MusicFileSelected(selectedMusicFile);
                mEventBus.post(musicFileSelectedEvent);
            }
        });
    }
}
