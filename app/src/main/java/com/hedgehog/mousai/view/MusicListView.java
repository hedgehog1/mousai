package com.hedgehog.mousai.view;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.hedgehog.mousai.adapter.MusicListAdapter;
import com.hedgehog.mousai.event.view.MusicListEvent;
import com.hedgehog.mousai.model.music.MusicFile;

import java.util.List;

import de.greenrobot.event.EventBus;


/**
 * View class for showing a list of music files
 */
public class MusicListView extends LinearLayout {

    private ListView         mListView;
    private MusicListAdapter mMusicListAdapter;
    private EventBus         mEventBus;


    public MusicListView(Context context) {
        super(context);
        init();
    }


    public MusicListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public MusicListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        mEventBus = EventBus.getDefault();
        mListView = new ListView(getContext());
        LinearLayout.LayoutParams layoutParams = new LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        addView(mListView, layoutParams);
    }


    /**
     * Set the music files that are going to be displayed
     */
    public void setMusicFiles(List<MusicFile> musicFilesList) {
        mMusicListAdapter = new MusicListAdapter(getContext(), musicFilesList);
        mListView.setAdapter(mMusicListAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MusicFile selectedMusicFile = mMusicListAdapter.getItem(position);
                MusicListEvent.MusicFileSelected musicFileSelectedEvent = new MusicListEvent.MusicFileSelected(
                        selectedMusicFile);
                mEventBus.post(musicFileSelectedEvent);
            }
        });
    }
}
