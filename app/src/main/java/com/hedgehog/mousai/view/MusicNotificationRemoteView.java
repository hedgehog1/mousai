package com.hedgehog.mousai.view;


import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.widget.RemoteViews;

import com.hedgehog.mousai.R;


/**
 * Class holding the remote views for a notification music controls.
 */
public class MusicNotificationRemoteView extends RemoteViews {

    public MusicNotificationRemoteView(Context context) {
        super(context.getPackageName(), R.layout.notification_music_player);
    }


    /**
     * Display the play button.
     */
    public void showPlay() {
        setImageViewResource(R.id.notification_control_play_pause, R.mipmap.ic_player_play);
    }


    /**
     * Display the pause button.
     */
    public void showPause() {
        setImageViewResource(R.id.notification_control_play_pause, R.mipmap.ic_player_pause);
    }


    /**
     * Register a {@link PendingIntent} that will be triggered upon clicking the next button in the notification.
     * 
     * @param intent
     *            the pending intent to trigger.
     */
    public void setOnNextClickedIntent(PendingIntent intent) {
        setOnClickPendingIntent(R.id.notification_control_next, intent);
    }


    /**
     * Register a {@link PendingIntent} that will be triggered upon clicking the previous button in the notification.
     *
     * @param intent
     *            the pending intent to trigger.
     */
    public void setOnPreviousClickedIntent(PendingIntent intent) {
        setOnClickPendingIntent(R.id.notification_control_previous, intent);
    }


    /**
     * Register a {@link PendingIntent} that will be triggered upon clicking the play/pause button in the notification.
     *
     * @param intent
     *            the pending intent to trigger.
     */
    public void setOnPlayPauseClickedIntent(PendingIntent intent) {
        setOnClickPendingIntent(R.id.notification_control_play_pause, intent);
    }


    /**
     * Register a {@link PendingIntent} that will be triggered upon clicking the close button in the notification.
     *
     * @param intent
     *            the pending intent to trigger.
     */
    public void setOnCloseClickedIntent(PendingIntent intent) {
        setOnClickPendingIntent(R.id.notification_control_close, intent);
    }


    /**
     * Set the art resource that should be displayed in the notification.
     * 
     * @param artResId
     *            the resource id of the art
     */
    public void setArt(int artResId) {
        setImageViewResource(R.id.notification_song_art, artResId);
    }


    /**
     * Set the art {@link Bitmap} that should be displayed in the notification.
     *
     * @param art
     *            the art {@link Bitmap}
     */
    public void setArt(Bitmap art) {
        setImageViewBitmap(R.id.notification_song_art, art);
    }

}
