package com.hedgehog.mousai.view;


import javax.inject.Inject;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.RemoteViews;

import com.hedgehog.mousai.R;
import com.hedgehog.mousai.application.MousaiApplication;
import com.hedgehog.mousai.theme.Theme;


public class WidgetRemoteView extends RemoteViews implements Themeable {

    @Inject
    Theme mTheme;


    public WidgetRemoteView(Context context) {
        super(context.getPackageName(), R.layout.widget_music_player);
        MousaiApplication.getViewComponent().inject(this);
        setTheme(mTheme);
    }


    /**
     * Display the play button.
     */
    public void showPlay() {
        setImageViewResource(R.id.widget_control_play_pause, R.drawable.ic_player_play);
    }


    /**
     * Display the pause button.
     */
    public void showPause() {
        setImageViewResource(R.id.widget_control_play_pause, R.drawable.ic_player_pause);
    }


    /**
     * Set the title that would be displayed in the widget
     */
    public void setTitle(CharSequence title) {
        setTextViewText(R.id.widget_song_title, title);

    }


    /**
     * Set the artist that would be displayed in the widget
     */
    public void setArtist(CharSequence artist) {
        setTextViewText(R.id.widget_song_artist, artist);
    }


    /**
     * Set the art resource that should be displayed in the widget.
     *
     * @param artResId
     *            the resource id of the art
     */
    public void setArt(int artResId) {
        setImageViewResource(R.id.widget_song_art, artResId);
    }


    /**
     * Set the art {@link Bitmap} that should be displayed in the widget.
     *
     * @param art
     *            the art {@link Bitmap}
     */
    public void setArt(Bitmap art) {
        setImageViewBitmap(R.id.widget_song_art, art);
    }


    public void setControlsColor(int color) {
        String setColorFilterMethodName = "setColorFilter";
        setInt(R.id.widget_control_play_pause, setColorFilterMethodName, color);
        setInt(R.id.widget_control_next, setColorFilterMethodName, color);
        setInt(R.id.widget_control_previous, setColorFilterMethodName, color);
    }


    public void setTextColor(int color) {
        setTextColor(R.id.widget_song_title, color);
        setTextColor(R.id.widget_song_artist, color);
    }


    public void setBackgroundColor(int color) {
        setInt(R.id.widget_container, "setBackgroundColor", color);
    }


    @Override
    public void setTheme(Theme theme) {
        if (theme != null) {
            mTheme = theme;
            setBackgroundColor(mTheme.getPrimaryColor());
            setControlsColor(mTheme.getPrimaryTextColor());
            setTextColor(mTheme.getPrimaryTextColor());
        }
    }
}
