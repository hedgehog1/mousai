package com.hedgehog.mousai.view;


import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.hedgehog.mousai.R;
import com.hedgehog.mousai.application.MousaiApplication;
import com.hedgehog.mousai.event.view.PlayerControlsEvent;
import com.hedgehog.mousai.theme.Theme;
import com.hedgehog.mousai.util.ColorUtils;
import com.hedgehog.mousai.util.StringUtils;
import com.hedgehog.mousai.view.widget.ThemeableToolbar;

import de.greenrobot.event.EventBus;

import javax.inject.Inject;


/**
 * A view for a media player. Contains buttons like "Play/Pause", "Previous", "Next", etc ... This class does not act on
 * any events nor does it have any business logic in it. The events are delegated and it is the client`s responsibility
 * to handle them.
 */
public class PlayerView extends RelativeLayout implements ToolbarView, Themeable {

    @Inject
    EventBus mEventBus;
    @Inject
    Theme    mTheme;

    private ThemeableToolbar mToolbar;
    private ImageButton      mPlayPause;
    private ImageButton      mPlayNext;
    private ImageButton      mPlayPrevious;
    private SeekBar          mSeekBar;
    private TextView         mElapsedTimeTextView;
    private TextView         mTotalTimeTextView;
    private LinearLayout     mNavigationControlsContainer;
    private RelativeLayout   mSeekBarContainer;


    public PlayerView(Context context) {
        super(context);
        init();
    }


    public PlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public PlayerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    /**
     * Display the play button.
     */
    public void showPlay() {
        mPlayPause.setImageResource(R.drawable.ic_player_play);
    }


    /**
     * Display the pause button.
     */
    public void showPause() {
        mPlayPause.setImageResource(R.drawable.ic_player_pause);
    }


    public void setProgress(int progress, int maxProgress) {
        if (maxProgress != mSeekBar.getMax()) {
            mSeekBar.setMax(maxProgress);
            mTotalTimeTextView.setText(StringUtils.formatTime(maxProgress));
        }

        mSeekBar.setProgress(progress);
        mElapsedTimeTextView.setText(StringUtils.formatTime(progress));
    }


    public void setPrimaryControlsColor(int color) {
        PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN);
        mPlayNext.setColorFilter(colorFilter);
        mPlayPause.setColorFilter(colorFilter);
        mPlayPrevious.setColorFilter(colorFilter);
        mElapsedTimeTextView.setTextColor(color);
        mTotalTimeTextView.setTextColor(color);
        mSeekBar.getThumb().setColorFilter(colorFilter);
    }


    public void setPrimaryBackgroundColor(int color) {
        mNavigationControlsContainer.setBackgroundColor(color);
        mSeekBarContainer.setBackgroundColor(color);
    }


    public void setSecondaryControlsColor(int color) {
        PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN);
        mSeekBar.getProgressDrawable().setColorFilter(colorFilter);
    }


    @Override
    public void setTheme(Theme theme) {
        if (theme != null) {
            mTheme = theme;
            setPrimaryControlsColor(mTheme.getPrimaryTextColor());
            setSecondaryControlsColor(mTheme.getSecondaryColor());

            // Make the backgrounds transparent 
            setPrimaryBackgroundColor(ColorUtils.setAlpha(theme.getPrimaryColor(), 0.9f));
            mToolbar.setTheme(new Theme(ColorUtils.setAlpha(theme.getPrimaryColor(), 0.95f), theme.getPrimaryTextColor(),
              theme.getSecondaryColor(), theme.getSecondaryTextColor()));
        }
    }


    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }


    private void init() {
        MousaiApplication.getViewComponent().inject(this);
        initToolbar();
        initViews();
        setTheme(mTheme);
    }


    private void initToolbar() {
        mToolbar = new ThemeableToolbar(getContext());
        LayoutParams toolbarLP = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        toolbarLP.addRule(ALIGN_PARENT_TOP);
        addView(mToolbar, toolbarLP);
    }


    private void initViews() {
        initNavigationControlsContainer();
        initSeekBarContainer();
    }


    private void initNavigationControlsContainer() {
        mNavigationControlsContainer = new LinearLayout(getContext());
        mNavigationControlsContainer.setOrientation(LinearLayout.HORIZONTAL);
        mNavigationControlsContainer.setId(R.id.music_player_navigation_controls_id);
        mNavigationControlsContainer.setPadding(0, 0, 0, 0);
        LayoutParams navigationControlsLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        navigationControlsLP.addRule(ALIGN_PARENT_BOTTOM);

        initPlayPreviousView();
        initPlayPauseView();
        initPlayNextView();

        addView(mNavigationControlsContainer, navigationControlsLP);
    }


    private void initPlayPreviousView() {
        mPlayPrevious = new ImageButton(getContext());
        mPlayPrevious.setId(R.id.music_player_play_previous_button_id);
        mPlayPrevious.setImageResource(R.drawable.ic_player_previous);
        mPlayPrevious.setBackgroundResource(android.R.color.transparent);
        mPlayPrevious.setScaleType(ImageView.ScaleType.FIT_CENTER);
        int size = (int) getContext().getResources().getDimension(R.dimen.music_player_controls_view_secondary_button_size);
        LinearLayout.LayoutParams playPreviousLP = new LinearLayout.LayoutParams(size, size);
        playPreviousLP.gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;
        playPreviousLP.weight = 1;
        mPlayPrevious.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mEventBus.post(new PlayerControlsEvent.Previous());
            }
        });
        mNavigationControlsContainer.addView(mPlayPrevious, playPreviousLP);
    }


    private void initPlayNextView() {
        mPlayNext = new ImageButton(getContext());
        mPlayNext.setId(R.id.music_player_play_next_button_id);
        mPlayNext.setImageResource(R.drawable.ic_player_next);
        mPlayNext.setBackgroundResource(android.R.color.transparent);
        mPlayNext.setScaleType(ImageView.ScaleType.FIT_CENTER);
        int size = (int) getContext().getResources().getDimension(R.dimen.music_player_controls_view_secondary_button_size);
        LinearLayout.LayoutParams playNextLP = new LinearLayout.LayoutParams(size, size);
        playNextLP.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
        playNextLP.weight = 1;
        mPlayNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mEventBus.post(new PlayerControlsEvent.Next());
            }
        });
        mNavigationControlsContainer.addView(mPlayNext, playNextLP);
    }


    private void initPlayPauseView() {
        mPlayPause = new ImageButton(getContext());
        mPlayPause.setId(R.id.music_player_play_pause_button_id);
        mPlayPause.setBackgroundResource(android.R.color.transparent);
        mPlayPause.setImageResource(R.drawable.ic_player_play);
        mPlayPause.setScaleType(ImageView.ScaleType.FIT_CENTER);
        int size = (int) getContext().getResources().getDimension(R.dimen.music_player_controls_view_primary_button_size);
        LinearLayout.LayoutParams playPauseLP = new LinearLayout.LayoutParams(size, size);
        playPauseLP.gravity = Gravity.CENTER;
        playPauseLP.weight = 1;
        mPlayPause.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mEventBus.post(new PlayerControlsEvent.PlayPause());
            }
        });
        mNavigationControlsContainer.addView(mPlayPause, playPauseLP);
    }


    private void initSeekBarContainer() {
        mSeekBarContainer = new RelativeLayout(getContext());
        mSeekBarContainer.setId(R.id.music_player_seek_bar_id);
        LayoutParams seekBarContainerLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        seekBarContainerLP.addRule(ABOVE, mNavigationControlsContainer.getId());

        mSeekBarContainer.setPadding(0, 0, 0, 0);

        initProgressBarView();
        initCurrentTimeView();
        initTotalTimeView();

        addView(mSeekBarContainer, seekBarContainerLP);
    }


    private void initProgressBarView() {
        mSeekBar = new SeekBar(getContext());
        mSeekBar.setId(R.id.music_player_seek_bar_id);

        LayoutParams progressBarLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mElapsedTimeTextView.setText(StringUtils.formatTime(progress));
            }


            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mEventBus.post(new PlayerControlsEvent.SeekBarTrackingStarted());
            }


            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mEventBus.post(new PlayerControlsEvent.ProgressChanged(seekBar.getProgress()));
            }
        });

        mSeekBarContainer.addView(mSeekBar, progressBarLP);
    }


    private void initCurrentTimeView() {
        mElapsedTimeTextView = new TextView(getContext());
        mElapsedTimeTextView.setId(R.id.music_player_elapsed_time_id);
        mElapsedTimeTextView.setTextColor(Color.WHITE);
        LayoutParams currentTimeLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        currentTimeLP.addRule(RelativeLayout.BELOW, mSeekBar.getId());
        currentTimeLP.addRule(RelativeLayout.ALIGN_LEFT, mSeekBar.getId());
        currentTimeLP
          .setMargins((int) getResources().getDimension(R.dimen.music_player_controls_view_seek_bar_text_margin), 0, 0, 0);

        mElapsedTimeTextView.setText(getResources().getString(R.string.inital_time));

        mSeekBarContainer.addView(mElapsedTimeTextView, currentTimeLP);
    }


    private void initTotalTimeView() {
        mTotalTimeTextView = new TextView(getContext());
        mTotalTimeTextView.setId(R.id.music_player_total_time_id);
        mTotalTimeTextView.setTextColor(Color.WHITE);
        LayoutParams totalTimeLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        totalTimeLP.addRule(RelativeLayout.BELOW, mSeekBar.getId());
        totalTimeLP.addRule(RelativeLayout.ALIGN_RIGHT, mSeekBar.getId());
        totalTimeLP
          .setMargins(0, 0, (int) getResources().getDimension(R.dimen.music_player_controls_view_seek_bar_text_margin), 0);

        mTotalTimeTextView.setText(getResources().getString(R.string.inital_time));

        mSeekBarContainer.addView(mTotalTimeTextView, totalTimeLP);
    }
}
