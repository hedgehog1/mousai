package com.hedgehog.mousai.view;


import android.content.Context;
import android.graphics.PixelFormat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.hedgehog.mousai.R;
import com.hedgehog.mousai.event.view.FloatingControlsEvent;

import de.greenrobot.event.EventBus;


/**
 * View representing floating controls for media player.
 */
public class FloatingControlsView extends FrameLayout {

    private ImageView mPlayPauseView;
    private boolean   mIsAttached;


    public FloatingControlsView(Context context) {
        super(context);
        init();
    }


    public FloatingControlsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public FloatingControlsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        mPlayPauseView = new ImageView(getContext());
        mPlayPauseView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new FloatingControlsEvent.PlayPause());
            }
        });
        mPlayPauseView.setImageResource(R.mipmap.ic_player_pause);
        FrameLayout.LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        addView(mPlayPauseView, lp);

    }


    /**
     * Display the pause button.
     */
    public void showPause() {
        mPlayPauseView.setImageResource(R.mipmap.ic_player_pause);
    }


    /**
     * Display the play button.
     */
    public void showPlay() {
        mPlayPauseView.setImageResource(R.mipmap.ic_player_play);
    }


    /**
     * Attach the view to the {@link WindowManager window manager} on the left over all screens.
     */
    public void attachToScreen() {
        if (!mIsAttached) {
            WindowManager.LayoutParams params = new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
            params.gravity = Gravity.LEFT;
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            windowManager.addView(this, params);
            mIsAttached = true;
        }
    }


    /**
     * Detach the view from the {@link WindowManager window manager}.
     */
    public void detachFromScreen() {
        if (mIsAttached) {
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            windowManager.removeView(this);
            mIsAttached = false;
        }
    }

}
