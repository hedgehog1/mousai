package com.hedgehog.mousai.view;


import android.content.Context;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.*;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.hedgehog.mousai.R;
import com.hedgehog.mousai.application.MousaiApplication;
import com.hedgehog.mousai.event.view.FloatingControlsEvent;
import com.hedgehog.mousai.theme.Theme;
import de.greenrobot.event.EventBus;

import javax.inject.Inject;


/**
 * View representing floating controls for media player.
 */
public class FloatingControlsView extends FrameLayout implements Themeable {

    @Inject
    EventBus mEventBus;
    @Inject
    Theme    mTheme;

    private ImageView     mPlayPauseView;
    private boolean       mIsAttached;
    private WindowManager mWindowManager;


    public FloatingControlsView(Context context) {
        super(context);
        init();
    }


    public FloatingControlsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public FloatingControlsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    @Override
    public void setTheme(Theme theme) {
        if (theme != null) {
            mTheme = theme;
            setPrimaryColor(mTheme.getPrimaryTextColor());
            setSecondaryColor(mTheme.getPrimaryColor());
        }
    }


    public void setPrimaryColor(int color) {
        mPlayPauseView.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
    }


    public void setSecondaryColor(int color) {
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.white_circle);
        drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        mPlayPauseView.setBackground(drawable);
    }


    /**
     * Display the pause button.
     */
    public void showPause() {
        mPlayPauseView.setImageResource(R.drawable.ic_player_pause);
    }


    /**
     * Display the play button.
     */
    public void showPlay() {
        mPlayPauseView.setImageResource(R.drawable.ic_player_play);
    }


    /**
     * Attach the view to the {@link WindowManager window manager} on the left over all screens.
     */
    public void attachToScreen() {
        if (!mIsAttached) {
            WindowManager.LayoutParams params = new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
            params.gravity = Gravity.LEFT;
            mWindowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            mWindowManager.addView(this, params);
            mIsAttached = true;

        }
    }


    /**
     * Detach the view from the {@link WindowManager window manager}.
     */
    public void detachFromScreen() {
        if (mIsAttached) {
            mWindowManager.removeView(this);
            mIsAttached = false;
        }
    }


    private void init() {
        MousaiApplication.getViewComponent().inject(this);
        mPlayPauseView = new ImageView(getContext());
        mPlayPauseView.setOnTouchListener(new DragViewOnTouchListener());
        mPlayPauseView.setImageResource(R.drawable.ic_player_pause);
        FrameLayout.LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        addView(mPlayPauseView, lp);
        setTheme(mTheme);
    }


    private class DragViewOnTouchListener implements OnTouchListener {

        private int   initialX;
        private int   initialY;
        private float initialTouchX;
        private float initialTouchY;


        @Override
        public boolean onTouch(View v, MotionEvent event) {
            WindowManager.LayoutParams params = (WindowManager.LayoutParams) getLayoutParams();
            switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initialX = params.x;
                initialY = params.y;
                initialTouchX = event.getRawX();
                initialTouchY = event.getRawY();
                return true;
            case MotionEvent.ACTION_UP:
                if (params.x == initialX && params.y == initialY) {
                    mEventBus.post(new FloatingControlsEvent.PlayPause());
                }
                return true;
            case MotionEvent.ACTION_MOVE:
                params.x = initialX + (int) (event.getRawX() - initialTouchX);
                params.y = initialY + (int) (event.getRawY() - initialTouchY);
                mWindowManager.updateViewLayout(FloatingControlsView.this, params);
                return true;
            }
            return false;
        }
    }

}
