package com.hedgehog.mousai.view.widget;


import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;

import com.hedgehog.mousai.R;
import com.hedgehog.mousai.theme.Theme;
import com.hedgehog.mousai.view.Themeable;


public class ThemeableToolbar extends Toolbar implements Themeable {

    public ThemeableToolbar(Context context) {
        super(context);
        init();
    }


    public ThemeableToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public ThemeableToolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    @Override
    public void setTheme(Theme theme) {
        setBackgroundColor(theme.getPrimaryColor());
        setTitleTextColor(theme.getPrimaryTextColor());
        setSubtitleTextColor(theme.getPrimaryTextColor());
    }

    private void init() {
        Resources res = getContext().getResources();
        setMinimumHeight((int) res.getDimension(R.dimen.toolbar_min_height));
        setPadding((int) res.getDimension(R.dimen.toolbar_padding_left), (int) res.getDimension(R.dimen.toolbar_padding_top),
          (int) res.getDimension(R.dimen.toolbar_padding_right), (int) res.getDimension(R.dimen.toolbar_padding_bottom));
    }
}
