package com.hedgehog.mousai.view;


import com.hedgehog.mousai.theme.Theme;


public interface Themeable {

    void setTheme(Theme theme);
}
