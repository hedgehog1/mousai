package com.hedgehog.mousai.controller.notification;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.hedgehog.mousai.R;
import com.hedgehog.mousai.constant.IntentAction;
import com.hedgehog.mousai.constant.IntentExtra;
import com.hedgehog.mousai.constant.IntentRequestCode;
import com.hedgehog.mousai.controller.service.MusicPlayerService;
import com.hedgehog.mousai.event.Event;
import com.hedgehog.mousai.event.controller.service.MusicPlayerServiceEvent;
import com.hedgehog.mousai.event.view.MusicNotificationEvent;
import com.hedgehog.mousai.util.service.EventBusProxyService;
import com.hedgehog.mousai.view.MusicNotificationRemoteView;

import de.greenrobot.event.EventBus;


/**
 * Notification that can be displayed along with an instance of {@link MusicPlayerService}.
 */
public class MusicNotification extends Notification {

    private MusicPlayerService          mService;
    private EventBus                    mEventBus;
    private int                         mId;
    private MusicNotificationRemoteView mView;


    public MusicNotification(MusicPlayerService service, int id) {
        mId = id;
        mService = service;
        mView = new MusicNotificationRemoteView(mService);
        contentView = mView;
        initContentView();
        icon = R.mipmap.ic_launcher;
        mEventBus = EventBus.getDefault();
        mEventBus.register(this);
    }


    /**
     * Needed to be called before destruction of this object to free resources. Also cancels the notification from the
     * notification bar.
     */
    public void release() {
        mEventBus.unregister(this);
        NotificationManager notificationManager = (NotificationManager) mService
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(mId);
        mService = null;
    }


    public void onEvent(MusicPlayerServiceEvent.PlaybackPaused event) {
        mView.showPlay();
        updateNotification();
    }


    public void onEvent(MusicPlayerServiceEvent.PlaybackStarted event) {
        mView.showPause();
        updateNotification();
    }


    public void onEvent(MusicNotificationEvent.Close event) {
        if (mService != null) {
            mService.stopSelf();
        }
    }


    private void updateNotification() {
        NotificationManager notificationManager = (NotificationManager) mService
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(mId, this);
    }


    private void initContentView() {
        mView.setOnPlayPauseClickedIntent(createOnClickPendingIntent(new MusicNotificationEvent.PlayPause(),
                IntentRequestCode.CODE_NOTIFICATION_PLAY_PAUSE));
        mView.setOnNextClickedIntent(createOnClickPendingIntent(new MusicNotificationEvent.Previous(),
                IntentRequestCode.CODE_NOTIFICATION_PREVIOUS));
        mView.setOnPreviousClickedIntent(createOnClickPendingIntent(new MusicNotificationEvent.Next(),
                IntentRequestCode.CODE_NOTIFICATION_NEXT));
        mView.setOnCloseClickedIntent(createOnClickPendingIntent(new MusicNotificationEvent.Close(),
                IntentRequestCode.CODE_NOTIFICATION_CLOSE));
    }


    private PendingIntent createOnClickPendingIntent(Event event, int requestCode) {
        Intent intent = new Intent(mService, EventBusProxyService.class);
        intent.setAction(IntentAction.INTENT_ACTION_PROXY_TO_EVENT_BUS);
        intent.putExtra(IntentExtra.INTENT_EXTRA_EVENT_OBJECT, event);
        return PendingIntent.getService(mService, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

}
