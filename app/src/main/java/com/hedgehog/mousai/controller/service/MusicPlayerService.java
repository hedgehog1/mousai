package com.hedgehog.mousai.controller.service;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import com.hedgehog.mousai.IMusicPlayer;
import com.hedgehog.mousai.controller.notification.MusicNotification;
import com.hedgehog.mousai.event.controller.service.MusicPlayerServiceEvent;
import com.hedgehog.mousai.event.view.MusicNotificationEvent;
import com.hedgehog.mousai.model.music.MusicFile;
import com.hedgehog.mousai.musicplayer.MusicPlayer;
import com.hedgehog.mousai.view.FloatingControlsView;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;


/**
 * Service that can play music files in the background.
 */
public class MusicPlayerService extends Service {

    private static final String TAG = MusicPlayerService.class.getCanonicalName();

    private static final int NOTIFICATION_ID = 1031;

    private ArrayList<MusicFile> mMusicFilesList;
    private MusicFile            mCurrentMusicFile;
    private int                  mCurrentMusicFileIndex;
    private MusicPlayer          mMusicPlayer;
    private MusicNotification    mNotification;
    private EventBus             mEventBus;
    private ServiceBinder        mBinder;
    private Thread               mMusicPlayerProgressObserverThread;


    @Override
    public void onCreate() {
        super.onCreate();
        mEventBus = EventBus.getDefault();
        mEventBus.register(this);
        mBinder = new ServiceBinder();
        mNotification = new MusicNotification(this, NOTIFICATION_ID);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startForeground(NOTIFICATION_ID, mNotification);
        return START_NOT_STICKY;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }


    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stopMusicPlayerObserver();
        mNotification.release();
        if (mMusicPlayer != null) {
            mMusicPlayer.release();
            mMusicPlayer = null;
        }

        mEventBus.post(new MusicPlayerServiceEvent.ServiceStopped());

        mMusicFilesList = null;
        mCurrentMusicFile = null;
        mEventBus.unregister(this);
        mEventBus = null;
        mBinder = null;
    }


    public void onEvent(MusicNotificationEvent.PlayPause event) {
        toggle();
    }


    public void onEvent(MusicNotificationEvent.Next event) {
        playNext();
    }


    public void onEvent(MusicNotificationEvent.Previous event) {
        playPrevious();
    }


    private void pause() {
        if (mMusicPlayer != null && mMusicPlayer.pause()) {
            mEventBus.post(new MusicPlayerServiceEvent.PlaybackPaused());
            stopMusicPlayerObserver();
        }
    }


    private void play() {
        if (mMusicPlayer != null && mMusicPlayer.play()) {
            startMusicPlayerObserver();
            mEventBus.post(new MusicPlayerServiceEvent.PlaybackStarted());
        }
    }


    private void playNext() {
        if (mMusicPlayer != null && mCurrentMusicFileIndex < mMusicFilesList.size() - 1) {
            stopMusicPlayerObserver();
            mCurrentMusicFileIndex++;
            mCurrentMusicFile = mMusicFilesList.get(mCurrentMusicFileIndex);
            setMusicFile(mCurrentMusicFile);
            play();
        }
    }


    private void playPrevious() {
        if (mMusicPlayer != null && mCurrentMusicFileIndex > 0) {
            stopMusicPlayerObserver();
            mCurrentMusicFileIndex--;
            mCurrentMusicFile = mMusicFilesList.get(mCurrentMusicFileIndex);
            setMusicFile(mCurrentMusicFile);
            play();
        }
    }


    private void seekTo(int progress) {
        if (mMusicPlayer != null) {
            mMusicPlayer.seekTo(progress);
            startMusicPlayerObserver();
        }
    }


    private void toggle() {
        if (mMusicPlayer != null && mMusicPlayer.isPlaying()) {
            pause();
        } else {
            play();
        }
    }


    private boolean isPlaying() {
        if (mMusicPlayer != null) {
            return mMusicPlayer.isPlaying();
        }
        return false;
    }


    private void init(List<MusicFile> musicFileList) {
        mMusicFilesList = new ArrayList<MusicFile>(musicFileList);
        if (mMusicFilesList != null && !mMusicFilesList.isEmpty()) {
            mCurrentMusicFileIndex = 0;
            mCurrentMusicFile = mMusicFilesList.get(mCurrentMusicFileIndex);
            if (mMusicPlayer == null) {
            mMusicPlayer = new MusicPlayer(mCurrentMusicFile, getApplicationContext());
            } else {
                mMusicPlayer.setMusicFile(mCurrentMusicFile);
            }
        } else {
            //No need to start the service if there are no music files to play
            stopSelf();
        }
    }


    private void setMusicFile(MusicFile file) {
        if (mMusicPlayer != null && file != null) {
            mMusicPlayer.setMusicFile(file);
        }
    }


    private void startMusicPlayerObserver() {
        if (mMusicPlayerProgressObserverThread == null) {
            mMusicPlayerProgressObserverThread = new Thread(new MusicPlayerProgressObserver(mMusicPlayer, mEventBus));
            mMusicPlayerProgressObserverThread.start();
        }
    }


    private void stopMusicPlayerObserver() {
        if (mMusicPlayerProgressObserverThread != null) {
            mMusicPlayerProgressObserverThread.interrupt();
            mMusicPlayerProgressObserverThread = null;
        }
    }

    /**
     * A binder that provides direct access to the service it was returned from.
     */
    public class ServiceBinder extends IMusicPlayer.Stub {

        @Override
        public void setPlayList(List<MusicFile> musicFileList) throws RemoteException {
            MusicPlayerService.this.init(musicFileList);
        }


        @Override
        public void play() throws RemoteException {
            MusicPlayerService.this.play();
        }


        @Override
        public void pause() throws RemoteException {
            MusicPlayerService.this.pause();
        }


        @Override
        public void toggle() throws RemoteException {
            MusicPlayerService.this.toggle();
        }


        @Override
        public void stop() throws RemoteException {
        }


        @Override
        public void playNext() throws RemoteException {
            MusicPlayerService.this.playNext();
        }


        @Override
        public void playPrevious() throws RemoteException {
            MusicPlayerService.this.playPrevious();
        }


        @Override
        public void seekTo(int progress) {
            MusicPlayerService.this.seekTo(progress);
        }


        @Override
        public boolean isPlaying() throws RemoteException {
            return MusicPlayerService.this.isPlaying();
        }


        @Override
        public void shuffle() throws RemoteException {

        }


        @Override
        public void setLooping(boolean looping) throws RemoteException {

        }
    }

    private class MusicPlayerProgressObserver implements Runnable {

        private boolean     mIsObserving;
        private EventBus    mLocalEventBus;
        private MusicPlayer mLocalMusicPlayer;


        /**
         * Starts progress tracking of specified {@link MusicPlayer} and sends message on progress update using the
         * provided {@link EventBus}
         *
         * @param {@link
         *            MusicPlayer musicPlayer}
         * @param {@link
         *            EventBus eventBus}
         */
        public MusicPlayerProgressObserver(MusicPlayer musicPlayer, EventBus eventBus) {
            mLocalMusicPlayer = musicPlayer;
            mLocalEventBus = eventBus;
            mIsObserving = true;
        }


        @Override
        public void run() {
            while (mIsObserving) {
                try {
                    sendProgressEvent();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    mIsObserving = false;
                }
            }
        }


        private void sendProgressEvent() {
            mLocalEventBus.post(new MusicPlayerServiceEvent.PlaybackProgress(mLocalMusicPlayer.getCurrentProgress(),
                    mLocalMusicPlayer.getMaxProgress()));
        }

    }
}
