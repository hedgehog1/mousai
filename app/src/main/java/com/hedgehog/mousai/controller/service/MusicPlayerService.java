package com.hedgehog.mousai.controller.service;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import com.hedgehog.mousai.IMusicPlayer;
import com.hedgehog.mousai.application.MousaiApplication;
import com.hedgehog.mousai.constant.IntentAction;
import com.hedgehog.mousai.controller.notification.MusicNotification;
import com.hedgehog.mousai.dal.MusicFilesDao;
import com.hedgehog.mousai.event.MusicPlayerEvent;
import com.hedgehog.mousai.event.controller.service.MusicPlayerServiceEvent;
import com.hedgehog.mousai.event.view.MusicNotificationEvent;
import com.hedgehog.mousai.model.music.MusicFile;
import com.hedgehog.mousai.musicplayer.MusicPlayer;
import com.hedgehog.mousai.util.CollectionUtils;

import java.util.ArrayList;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;


/**
 * Service that can play music files in the background.
 */
public class MusicPlayerService extends Service {

    private static final String TAG = MusicPlayerService.class.getCanonicalName();
    private static final int NOTIFICATION_ID = 1031;
    private static final int FIVE_SECONDS = 5 * 1000;

    @Inject
    EventBus mEventBus;
    @Inject
    MusicFilesDao mMusicFilesDao;

    private MusicPlayer mMusicPlayer;
    private MusicNotification mNotification;
    private ServiceBinder mBinder;
    private Thread mMusicPlayerProgressObserverThread;


    @Override
    public void onCreate() {
        super.onCreate();
        MousaiApplication.getControllerComponent().inject(this);
        mEventBus.register(this);
        mBinder = new ServiceBinder();
        mNotification = new MusicNotification(this, NOTIFICATION_ID);
        init();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sendBroadcast(new Intent(IntentAction.INTENT_ACTION_BROADCAST_MUSIC_PLAYER_STARTED));
        startForeground(NOTIFICATION_ID, mNotification);
        return START_NOT_STICKY;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }


    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stopMusicPlayerObserver();
        mNotification.release();
        if (mMusicPlayer != null) {
            mMusicPlayer.release();
            mMusicPlayer = null;
        }

        mEventBus.unregister(this);
        mEventBus = null;
        mBinder = null;
    }


    public void onEvent(MusicNotificationEvent.PlayPause event) {
        toggle();
    }


    public void onEvent(MusicNotificationEvent.Next event) {
        playNext();
    }


    public void onEvent(MusicNotificationEvent.Previous event) {
        playPrevious();
    }


    public void onEvent(MusicPlayerEvent.PlaybackCompleted event) {
        playNext();
    }

    private void pause() {
        if (mMusicPlayer != null && mMusicPlayer.pause()) {
            stopMusicPlayerObserver();
        }
    }


    private void play() {
        if (mMusicPlayer != null) {
            if (isPlaying()) {
                stopMusicPlayerObserver();
            }
            if (mMusicFilesDao.getCurrentMusicFile() != null && !mMusicFilesDao.getCurrentMusicFile().equals(mMusicPlayer.getMusicFile())) {
                mMusicPlayer.setMusicFile(mMusicFilesDao.getCurrentMusicFile());
                mEventBus.post(new MusicPlayerServiceEvent.MusicFileChanged(mMusicFilesDao.getCurrentMusicFile()));
            }
            mMusicPlayer.play();
            startMusicPlayerObserver();
        }
    }


    private void playNext() {
        int currentMusicFileIndex = mMusicFilesDao.getCurrentMusicFileIndex();
        ArrayList<MusicFile> currentMusicFileList = mMusicFilesDao.getCurrentMusicFilesList();

        if (mMusicPlayer != null && currentMusicFileIndex < currentMusicFileList.size() - 1) {

            stopMusicPlayerObserver();
            MusicFile currentMusicFile = currentMusicFileList.get(currentMusicFileIndex + 1);
            mMusicFilesDao.setCurrentMusicFile(currentMusicFile);
            setMusicFile(currentMusicFile);
            play();

        } else {
            pause();
        }
    }


    private void playPrevious() {
        if (mMusicPlayer != null) {
            int currentMusicFileIndex = mMusicFilesDao.getCurrentMusicFileIndex();
            if (currentMusicFileIndex > 0 && mMusicPlayer.getCurrentProgress() < FIVE_SECONDS) {
                stopMusicPlayerObserver();
                MusicFile currentMusicFile = mMusicFilesDao.getCurrentMusicFilesList().get(currentMusicFileIndex - 1);
                mMusicFilesDao.setCurrentMusicFile(currentMusicFile);
                setMusicFile(currentMusicFile);
                play();
            } else {
                seekTo(0);
            }
        }
    }


    private void seekTo(int progress) {
        if (mMusicPlayer != null) {
            mMusicPlayer.seekTo(progress);
            startMusicPlayerObserver();
        }
    }


    private void toggle() {
        if (mMusicPlayer != null && mMusicPlayer.isPlaying()) {
            pause();
        } else {
            play();
        }
    }


    private boolean isPlaying() {
        if (mMusicPlayer != null) {
            return mMusicPlayer.isPlaying();
        }
        return false;
    }


    private long getProgress() {
        if (mMusicPlayer != null) {
            return mMusicPlayer.getCurrentProgress();
        }

        return 0;
    }


    private long getMaxProgress() {
        if (mMusicPlayer != null) {
            return mMusicPlayer.getMaxProgress();
        }

        return 0;
    }


    private void init() {
        ArrayList<MusicFile> musicFilesList = mMusicFilesDao.getCurrentMusicFilesList();

        //Check if we have properly set music files list and music file to play
        if (!CollectionUtils.isNullOrEmpty(musicFilesList) && mMusicFilesDao.getCurrentMusicFile() != null) {
            if (mMusicPlayer == null) {
                mMusicPlayer = new MusicPlayer(mMusicFilesDao.getCurrentMusicFile());
            } else {
                mMusicPlayer.setMusicFile(mMusicFilesDao.getCurrentMusicFile());
            }

            //TODO: We can inject the Music files DAO where listening for this event 
            mEventBus.post(new MusicPlayerServiceEvent.MusicFileChanged(mMusicFilesDao.getCurrentMusicFile()));
        } else {
            //No need to start the service if there are no music files to play
            stopSelf();
        }
    }


    private void setMusicFile(MusicFile file) {
        if (mMusicPlayer != null && file != null) {
            mMusicPlayer.setMusicFile(file);
            //TODO: We can inject the Music files DAO where listening for this event so we do not need to add the music file in the event object
            mEventBus.post(new MusicPlayerServiceEvent.MusicFileChanged(mMusicFilesDao.getCurrentMusicFile()));
        }
    }


    private void startMusicPlayerObserver() {
        if (mMusicPlayerProgressObserverThread == null) {
            mMusicPlayerProgressObserverThread = new Thread(new MusicPlayerProgressObserver(mMusicPlayer, mEventBus));
            mMusicPlayerProgressObserverThread.start();
        }
    }


    private void stopMusicPlayerObserver() {
        if (mMusicPlayerProgressObserverThread != null) {
            mMusicPlayerProgressObserverThread.interrupt();
            mMusicPlayerProgressObserverThread = null;
        }
    }

    /**
     * A binder that provides direct access to the service it was returned from.
     */
    public class ServiceBinder extends IMusicPlayer.Stub {

        @Override
        public void play() throws RemoteException {
            MusicPlayerService.this.play();
        }


        @Override
        public void pause() throws RemoteException {
            MusicPlayerService.this.pause();
        }


        @Override
        public void toggle() throws RemoteException {
            MusicPlayerService.this.toggle();
        }


        @Override
        public void stop() throws RemoteException {
        }


        @Override
        public void playNext() throws RemoteException {
            MusicPlayerService.this.playNext();
        }


        @Override
        public void playPrevious() throws RemoteException {
            MusicPlayerService.this.playPrevious();
        }


        @Override
        public void seekTo(int progress) {
            MusicPlayerService.this.seekTo(progress);
        }


        @Override
        public boolean isPlaying() throws RemoteException {
            return MusicPlayerService.this.isPlaying();
        }


        @Override
        public void shuffle() throws RemoteException {

        }


        @Override
        public void setLooping(boolean looping) throws RemoteException {

        }


        @Override
        public long getProgress() {
            return MusicPlayerService.this.getProgress();
        }


        @Override
        public long getMaxProgress() {
            return MusicPlayerService.this.getMaxProgress();
        }
    }

    private class MusicPlayerProgressObserver implements Runnable {

        private boolean mIsObserving;
        private EventBus mLocalEventBus;
        private MusicPlayer mLocalMusicPlayer;


        /**
         * Starts progress tracking of specified {@link MusicPlayer} and sends message on progress update using the
         * provided {@link EventBus}
         *
         * @param {@link MusicPlayer musicPlayer}
         * @param {@link EventBus eventBus}
         */
        public MusicPlayerProgressObserver(MusicPlayer musicPlayer, EventBus eventBus) {
            mLocalMusicPlayer = musicPlayer;
            mLocalEventBus = eventBus;
            mIsObserving = true;
        }


        @Override
        public void run() {
            while (mIsObserving) {
                try {
                    sendProgressEvent();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    mIsObserving = false;
                }
            }
        }


        private void sendProgressEvent() {
            mLocalEventBus.post(new MusicPlayerServiceEvent.PlaybackProgress(mLocalMusicPlayer.getCurrentProgress(),
                    mLocalMusicPlayer.getMaxProgress()));
        }

    }
}
