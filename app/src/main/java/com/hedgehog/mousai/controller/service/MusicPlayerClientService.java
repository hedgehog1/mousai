package com.hedgehog.mousai.controller.service;


import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.hedgehog.mousai.IMusicPlayer;
import com.hedgehog.mousai.util.SystemServicesUtils;


/**
 * Service that connects to {@link MusicPlayerService} and provides interface for interacting with the it. The service
 * starts only if {@link MusicPlayerService} is already running and its alive while the player service is not stopped
 * (unless this service is explicitly stopped).
 */
public class MusicPlayerClientService extends Service {

    private IMusicPlayer      mMusicPlayerService;
    private boolean           mIsMusicPlayerServiceBound;
    private ServiceConnection mMusicPlayerServiceConnection;
    private BindMode          mBindMode;


    @Override
    public void onCreate() {
        super.onCreate();
        mMusicPlayerServiceConnection = new MusicPlayerServiceConnection();
        mBindMode = BindMode.SIMPLE_BIND;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mBindMode = onStartCommandBindMode();
        switch (mBindMode) {

        case BIND_SYNC_LIFECYCLE:
            if (!SystemServicesUtils.isServiceRunning(getApplicationContext(), MusicPlayerService.class)) {
                stopSelf();
            } else {
                bindToMusicPlayerService();
            }
            break;
        case START_AND_BIND:
            startMusicPlayerService();
        case SIMPLE_BIND:
            bindToMusicPlayerService();
            break;
        }
        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindFromMusicPlayerService();
        mMusicPlayerServiceConnection = null;
        mMusicPlayerService = null;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @NonNull
    protected BindMode onStartCommandBindMode() {
        return mBindMode;
    }


    /**
     * Check whether the service is bound to a {@link MusicPlayerService}.
     */
    protected boolean isBoundToMusicPlayer() {
        return mIsMusicPlayerServiceBound;
    }


    /**
     * Obtain the {@link MusicPlayerService}, that this service is bound to. Can be <code>null</code> if the
     * {@link MusicPlayerService} is not bound.
     * 
     */
    @Nullable
    protected IMusicPlayer getMusicPlayerService() {
        return mMusicPlayerService;
    }


    /**
     * Called upon connecting to the {@link MusicPlayerService}.
     * 
     * @param musicPlayer
     *            the {@link MusicPlayerService} that this service is bound to
     */
    protected void onMusicPlayerConnected(IMusicPlayer musicPlayer) {
    }


    /**
     * Called upon disconnecting from the {@link MusicPlayerService}. After this call the service will stop itself.
     */
    protected void onMusicPlayerDisconnected() {
    }


    private void startMusicPlayerService() {
        Intent musicPlayerStartIntent = new Intent(this, MusicPlayerService.class);
        startService(musicPlayerStartIntent);
    }


    private void bindToMusicPlayerService() {
        if (!mIsMusicPlayerServiceBound) {
            Intent musicPlayerBindIntent = new Intent(this, MusicPlayerService.class);
            bindService(musicPlayerBindIntent, mMusicPlayerServiceConnection, BIND_WAIVE_PRIORITY);
        }
    }


    private void unbindFromMusicPlayerService() {
        if (mIsMusicPlayerServiceBound) {
            unbindService(mMusicPlayerServiceConnection);
        }
    }

    /**
     * Specifies the binding flow for the service.
     */
    protected enum BindMode {
        /**
         * Binds this service only if {@link MusicPlayerService} is already running, otherwise stops itself. The service
         * will stop itself when the {@link MusicPlayerService} stops.
         */
        BIND_SYNC_LIFECYCLE, /**
                              * Binds this service to {@link MusicPlayerService} not syncing the lifecycle of this
                              * service and {@link MusicPlayerService}.
                              */
        SIMPLE_BIND, /**
                      * Starts {@link MusicPlayerService} and binds this service to it .This does not sync the lifecycle
                      * of this service and {@link MusicPlayerService}.
                      */
        START_AND_BIND
    }

    private class MusicPlayerServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mMusicPlayerService = IMusicPlayer.Stub.asInterface(service);
            mIsMusicPlayerServiceBound = true;
            onMusicPlayerConnected(mMusicPlayerService);
        }


        @Override
        public void onServiceDisconnected(ComponentName name) {
            unbindFromMusicPlayerService();
            mIsMusicPlayerServiceBound = false;
            onMusicPlayerDisconnected();
            if (mBindMode == BindMode.BIND_SYNC_LIFECYCLE) {
                stopSelf();
            }
        }
    }
}
