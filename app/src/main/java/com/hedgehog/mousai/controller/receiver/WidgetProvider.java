package com.hedgehog.mousai.controller.receiver;


import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.hedgehog.mousai.constant.IntentAction;
import com.hedgehog.mousai.controller.service.WidgetUpdateService;


/**
 * {@link android.content.BroadcastReceiver} that starts the {@link WidgetUpdateService} when appropriate in order to
 * sync the screen widget with the {@link com.hedgehog.mousai.controller.service.MusicPlayerService}.
 */
public class WidgetProvider extends AppWidgetProvider {

    @Override
    public void onReceive(@NonNull Context context, @NonNull Intent intent) {
        if (intent.getAction().equals(IntentAction.INTENT_ACTION_BROADCAST_MUSIC_PLAYER_STARTED)) {
            if (AppWidgetManager.getInstance(context)
                    .getAppWidgetIds(new ComponentName(context, WidgetProvider.class)).length > 0) {
                startWidgetUpdateService(context);
            }
        }
        super.onReceive(context, intent);
    }


    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        startWidgetUpdateService(context);
    }


    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        stopWidgetUpdateService(context);
    }


    private void startWidgetUpdateService(Context context) {
        Intent intent = new Intent(context.getApplicationContext(), WidgetUpdateService.class);
        context.startService(intent);
    }


    private void stopWidgetUpdateService(Context context) {
        Intent intent = new Intent(context.getApplicationContext(), WidgetUpdateService.class);
        context.stopService(intent);
    }
}
