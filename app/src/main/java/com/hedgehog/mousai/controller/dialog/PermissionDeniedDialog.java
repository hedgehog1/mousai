package com.hedgehog.mousai.controller.dialog;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import com.hedgehog.mousai.R;


public class PermissionDeniedDialog {

    private Activity mActivity;


    public PermissionDeniedDialog(Activity activity) {
        mActivity = activity;
    }


    public void show(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(mActivity.getString(R.string.dialog_button_settings),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openAppSettingsScreen();
                    }
                }).setNegativeButton(mActivity.getString(R.string.dialog_button_cancel), null).show();
    }


    private void openAppSettingsScreen() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", mActivity.getPackageName(), null);
        intent.setData(uri);
        mActivity.startActivity(intent);
    }
}
