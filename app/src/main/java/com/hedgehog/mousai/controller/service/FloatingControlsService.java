package com.hedgehog.mousai.controller.service;


import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import com.hedgehog.mousai.IMusicPlayer;
import com.hedgehog.mousai.event.controller.service.MusicPlayerServiceEvent;
import com.hedgehog.mousai.event.view.FloatingControlsEvent;
import com.hedgehog.mousai.util.SystemServicesUtils;
import com.hedgehog.mousai.view.FloatingControlsView;

import de.greenrobot.event.EventBus;


/**
 * Service that controls the floating controls attached on top of the window.
 */
public class FloatingControlsService extends Service {

    private FloatingControlsView mView;
    private IMusicPlayer         mMusicPlayerService;
    private boolean              mIsMusicPlayerServiceBound;
    private ServiceConnection    mMusicPlayerServiceConnection;
    private EventBus             mEventBus;


    @Override
    public void onCreate() {
        super.onCreate();
        mEventBus = EventBus.getDefault();
        mEventBus.register(this);
        mMusicPlayerServiceConnection = new MusicPlayerServiceConnection();
        mView = new FloatingControlsView(this);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (SystemServicesUtils.isServiceRunning(getApplicationContext(), MusicPlayerService.class)) {
            bindToMusicPlayerService();
        } else {
            stopSelf();
        }
        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindFromMusicPlayerService();
        mEventBus.unregister(this);
        mView.detachFromScreen();
        mMusicPlayerServiceConnection = null;
        mView = null;
        mEventBus = null;
        mMusicPlayerService = null;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public void onEventMainThread(MusicPlayerServiceEvent.PlaybackStarted event) {
        mView.showPause();
    }


    public void onEventMainThread(MusicPlayerServiceEvent.PlaybackPaused event) {
        mView.showPlay();
    }


    public void onEvent(FloatingControlsEvent.PlayPause event) throws RemoteException {
        if (mIsMusicPlayerServiceBound) {
            mMusicPlayerService.toggle();
        }
    }


    public void onEvent(MusicPlayerServiceEvent.ServiceStopped event) {
        stopSelf();
    }


    private void bindToMusicPlayerService() {
        if (!mIsMusicPlayerServiceBound) {
            Intent musicPlayerBindIntent = new Intent(this, MusicPlayerService.class);
            bindService(musicPlayerBindIntent, mMusicPlayerServiceConnection, BIND_WAIVE_PRIORITY);
        }
    }


    private void unbindFromMusicPlayerService() {
        unbindService(mMusicPlayerServiceConnection);
    }

    private class MusicPlayerServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mMusicPlayerService = IMusicPlayer.Stub.asInterface(service);
            try {
                if (mMusicPlayerService.isPlaying()) {
                    mView.showPause();
                } else {
                    mView.showPlay();
                }

                mIsMusicPlayerServiceBound = true;
                mView.attachToScreen();
            } catch (RemoteException e) {
                // If we are unable to show to proper state of the controls it is better to shut them
                stopSelf();
            }
        }


        @Override
        public void onServiceDisconnected(ComponentName name) {
            mIsMusicPlayerServiceBound = false;
            stopSelf();
        }
    }
}
