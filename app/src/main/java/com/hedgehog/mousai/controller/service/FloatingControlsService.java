package com.hedgehog.mousai.controller.service;


import javax.inject.Inject;

import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.hedgehog.mousai.IMusicPlayer;
import com.hedgehog.mousai.application.MousaiApplication;
import com.hedgehog.mousai.event.MusicPlayerEvent;
import com.hedgehog.mousai.event.theme.ThemeEvent;
import com.hedgehog.mousai.event.view.FloatingControlsEvent;
import com.hedgehog.mousai.view.FloatingControlsView;

import de.greenrobot.event.EventBus;


/**
 * Service that controls the floating controls attached on top of the window.
 */
public class FloatingControlsService extends MusicPlayerClientService {

    @Inject
    EventBus                     mEventBus;
    private FloatingControlsView mView;


    @Override
    public void onCreate() {
        super.onCreate();
        MousaiApplication.getControllerComponent().inject(this);
        mEventBus.register(this);
        mView = new FloatingControlsView(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mEventBus.unregister(this);
        mView.detachFromScreen();

        mEventBus = null;
        mView = null;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public void onEventMainThread(MusicPlayerEvent.PlaybackStarted event) {
        mView.showPause();
    }


    public void onEventMainThread(MusicPlayerEvent.PlaybackPaused event) {
        mView.showPlay();
    }


    public void onEventMainThread(ThemeEvent.ThemeChanged event) {
        mView.setTheme(event.newTheme);
    }


    public void onEvent(FloatingControlsEvent.PlayPause event) {
        if (isBoundToMusicPlayer()) {
            try {
                IMusicPlayer musicPlayer = getMusicPlayerService();
                if (musicPlayer != null) {
                    musicPlayer.toggle();
                }
            } catch (RemoteException e) {
                // noting to do
            }
        }
    }


    @Override
    protected void onMusicPlayerConnected(IMusicPlayer musicPlayer) {
        try {
            if (musicPlayer.isPlaying()) {
                mView.showPause();
            } else {
                mView.showPlay();
            }

            mView.attachToScreen();
        } catch (RemoteException e) {
            // If we are unable to show the proper state of the controls it is better to shut them
            stopSelf();
        }
    }


    @NonNull
    @Override
    protected BindMode onStartCommandBindMode() {
        return BindMode.BIND_SYNC_LIFECYCLE;
    }
}
