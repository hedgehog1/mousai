package com.hedgehog.mousai.controller.service;


import javax.inject.Inject;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.hedgehog.mousai.IMusicPlayer;
import com.hedgehog.mousai.R;
import com.hedgehog.mousai.application.MousaiApplication;
import com.hedgehog.mousai.controller.receiver.WidgetProvider;
import com.hedgehog.mousai.dal.MusicFilesDao;
import com.hedgehog.mousai.event.MusicPlayerEvent;
import com.hedgehog.mousai.event.controller.service.MusicPlayerServiceEvent;
import com.hedgehog.mousai.event.theme.ThemeEvent;
import com.hedgehog.mousai.model.music.MusicFile;
import com.hedgehog.mousai.util.ArtStore;
import com.hedgehog.mousai.view.WidgetRemoteView;

import de.greenrobot.event.EventBus;


/**
 * {@link android.app.Service} that updates all screen widgets provided by {@link WidgetProvider} and syncs it with the
 * {@link MusicPlayerService}.
 */
public class WidgetUpdateService extends MusicPlayerClientService {

    @Inject
    EventBus                 mEventBus;
    @Inject
    AppWidgetManager         mAppWidgetManager;
    @Inject
    MusicFilesDao            mMusicFilesDao;
    private WidgetRemoteView mView;
    private ComponentName    mWidgetComponentName;


    @Override
    public void onCreate() {
        super.onCreate();
        MousaiApplication.getControllerComponent().inject(this);
        mEventBus.register(this);
        mView = new WidgetRemoteView(getApplicationContext());
        mWidgetComponentName = new ComponentName(getApplicationContext(), WidgetProvider.class);
    }


    @Override
    protected void onMusicPlayerConnected(IMusicPlayer musicPlayer) {
        try {
            if (musicPlayer.isPlaying()) {
                mView.showPause();
            } else {
                mView.showPlay();
            }

            updateMusicFile();
            partiallyUpdateWidget();
        } catch (RemoteException e) {
            // nothing to do
        }
    }


    public void onEventMainThread(MusicPlayerEvent.PlaybackStarted event) {
        mView.showPause();
        partiallyUpdateWidget();
    }


    public void onEventMainThread(MusicPlayerEvent.PlaybackPaused event) {
        mView.showPlay();
        partiallyUpdateWidget();
    }


    public void onEventMainThread(MusicPlayerServiceEvent.MusicFileChanged event) {
        updateMusicFile();
        partiallyUpdateWidget();
    }


    public void onEventMainThread(ThemeEvent.ThemeChanged event) {
        mView.setTheme(event.newTheme);
        partiallyUpdateWidget();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mView.showPlay();
        partiallyUpdateWidget();

        mEventBus.unregister(this);
        mEventBus = null;
        mView = null;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @NonNull
    @Override
    protected BindMode onStartCommandBindMode() {
        updateWidget();
        return BindMode.BIND_SYNC_LIFECYCLE;
    }


    private void updateMusicFile() {
        MusicFile currentMusicFile = mMusicFilesDao.getCurrentMusicFile();
        Bitmap art = ArtStore.getAlbumArtThumbnail(currentMusicFile, 250);
        if (art != null) {
            mView.setArt(art);
        } else {
            mView.setArt(R.mipmap.ic_launcher);
        }
        mView.setTitle(currentMusicFile.getTitle());
        mView.setArtist(currentMusicFile.getArtist());
    }


    private void partiallyUpdateWidget() {
        int[] allWidgetIds = mAppWidgetManager.getAppWidgetIds(mWidgetComponentName);

        for (int widgetId : allWidgetIds) {

            mAppWidgetManager.partiallyUpdateAppWidget(widgetId, mView);
        }
    }


    private void updateWidget() {
        int[] allWidgetIds = mAppWidgetManager.getAppWidgetIds(mWidgetComponentName);

        for (int widgetId : allWidgetIds) {

            mAppWidgetManager.updateAppWidget(widgetId, mView);
        }
    }
}
