package com.hedgehog.mousai.controller.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.hedgehog.mousai.R;
import com.hedgehog.mousai.constant.IntentAction;
import com.hedgehog.mousai.constant.IntentExtra;
import com.hedgehog.mousai.dal.MusicFilesDao;
import com.hedgehog.mousai.event.view.MusicListEvent;
import com.hedgehog.mousai.model.music.MusicFile;
import com.hedgehog.mousai.view.MusicListView;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;


/**
 * Controller activity being used to get all music media files on the device and pass them to the corresponding view to
 * show them. This controller is responsible for maintaining all user interactions of MusicListView
 */
public class MusicListActivity extends AppCompatActivity {

    private static final String TAG = MusicListActivity.class.getCanonicalName();

    private MusicFilesDao mMusicFilesDao;
    private MusicListView mMusicListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_list);
        EventBus.getDefault().register(this);

        mMusicFilesDao = new MusicFilesDao(getApplicationContext());
        initMusicListView();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    public void onEvent(MusicListEvent.MusicFileSelected event) {
        MusicFile musicFile = event.musicFile;
        Intent intent = new Intent(this, PlayerActivity.class);
        intent.setAction(IntentAction.INTENT_ACTION_PLAY_MUSIC_FILE);
        intent.putExtra(IntentExtra.INTENT_EXTRA_MUSIC_FILES, getMusicFilesList(musicFile));
        startActivity(intent);
    }


    private ArrayList<MusicFile> getMusicFilesList(MusicFile musicFile) {
        ArrayList<MusicFile> playList = new ArrayList<MusicFile>();
        List<MusicFile> allMusicFiles = mMusicFilesDao.getMusicFilesList();
        int startIndex = allMusicFiles.indexOf(musicFile);
        int currentIndex = startIndex;
        do {
            playList.add(allMusicFiles.get(currentIndex));
            currentIndex = (currentIndex + 1) % allMusicFiles.size();
        } while (currentIndex != startIndex);

        return playList;
    }


    private void initMusicListView() {
        mMusicListView = (MusicListView) findViewById(R.id.music_list_view_id);
        mMusicListView.setMusicFiles(mMusicFilesDao.getMusicFilesList());
    }
}
