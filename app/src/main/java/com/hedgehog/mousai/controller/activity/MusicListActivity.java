package com.hedgehog.mousai.controller.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.hedgehog.mousai.R;
import com.hedgehog.mousai.adapter.MusicListAdapter;
import com.hedgehog.mousai.application.MousaiApplication;
import com.hedgehog.mousai.async.GetAllMusicFilesTask;
import com.hedgehog.mousai.constant.IntentAction;
import com.hedgehog.mousai.constant.Path;
import com.hedgehog.mousai.dal.MusicFilesDao;
import com.hedgehog.mousai.event.view.MusicListEvent;
import com.hedgehog.mousai.model.music.MusicFile;
import com.hedgehog.mousai.util.ArtStore;
import com.hedgehog.mousai.util.PermissionsHelper;
import com.hedgehog.mousai.view.MusicListView;

import de.greenrobot.event.EventBus;

import java.util.List;

import javax.inject.Inject;


/**
 * Controller activity being used to get all music media files on the device and pass them to the corresponding view to
 * show them. This controller is responsible for maintaining all user interactions of MusicListView
 */
public class MusicListActivity extends AppCompatActivity {

    @Inject
    MusicFilesDao mMusicFilesDao;
    @Inject
    EventBus      mEventBus;
    private MusicListView     mMusicListView;
    private PermissionsHelper mPermissionHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_list);
        MousaiApplication.getControllerComponent().inject(this);
        mPermissionHelper = new PermissionsHelper(this);
        initMusicListView();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mEventBus.isRegistered(this)) {
            mEventBus.unregister(this);
        }
    }


    public void onEventMainThread(MusicListEvent.MusicFileSelected event) {
        MusicFile musicFile = event.musicFile;
        Intent intent = new Intent(this, PlayerActivity.class);
        intent.setAction(IntentAction.INTENT_ACTION_PLAY_MUSIC_FILE);
        mMusicFilesDao.setStartingMusicFile(musicFile);
        startActivity(intent);
    }


    private void initMusicListView() {
        mPermissionHelper.actionOnStoragePermission(new PermissionsHelper.OnStoragePermissionActionListener() {

            @Override
            public void onStoragePermissionGranted() {
                new GetAllMusicFilesTask() {

                    @Override
                    protected void onPostExecute(List<MusicFile> musicFiles) {
                        mMusicListView = (MusicListView) findViewById(R.id.music_list_view_id);
                        MusicListAdapter adapter = new MusicListAdapter(getApplicationContext(), musicFiles);
                        adapter.setImageLoader(
                          new ArtStore(MusicListActivity.this, R.mipmap.ic_launcher, Path.RELATIVE_IMAGE_CACHE, 0.25f)
                            .getFileArtLoader(ArtStore.Format.THUMBNAIL));
                        mMusicListView.setAdapter(adapter);
                    }
                }.execute();
            }


            @Override
            public void onStoragePermissionDenied() {
                finish();
            }
        });
    }
}