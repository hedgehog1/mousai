package com.hedgehog.mousai.controller.activity;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;

import com.hedgehog.mousai.IMusicPlayer;
import com.hedgehog.mousai.R;
import com.hedgehog.mousai.constant.IntentAction;
import com.hedgehog.mousai.constant.IntentExtra;
import com.hedgehog.mousai.controller.service.FloatingControlsService;
import com.hedgehog.mousai.controller.service.MusicPlayerService;
import com.hedgehog.mousai.event.controller.service.MusicPlayerServiceEvent;
import com.hedgehog.mousai.event.view.FloatingControlsEvent;
import com.hedgehog.mousai.event.view.PlayerControlsEvent;
import com.hedgehog.mousai.model.music.MusicFile;
import com.hedgehog.mousai.util.SystemServicesUtils;
import com.hedgehog.mousai.view.PlayerControlsView;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

import static android.content.Intent.ACTION_VIEW;


public class PlayerActivity extends AppCompatActivity {

    private static final String TAG = PlayerActivity.class.getCanonicalName();

    private PlayerControlsView   mView;
    private ArrayList<MusicFile> mMusicFilesList;
    private IMusicPlayer         mMusicPlayerService;
    private boolean              mIsMusicPlayerServiceBound;
    private ServiceConnection    mMusicPlayerServiceConnection;
    private boolean              mIsSeeking;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        mView = (PlayerControlsView) findViewById(R.id.player_controls_view);
        EventBus.getDefault().register(this);
        mIsMusicPlayerServiceBound = false;
        mMusicPlayerServiceConnection = new MusicPlayerServiceConnection();

        handleOnCreateIntent();
        stopFloatingControlsService();
    }


    @Override
    protected void onResume() {
        super.onResume();
        bindToMusicPlayerService();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mIsMusicPlayerServiceBound) {
            unbindService(mMusicPlayerServiceConnection);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        //TODO: start the floating controls when most appropriate
        // here it is started also in case of locking the screen while in this activity
        startFloatingControlsService();
    }


    public void onEventMainThread(PlayerControlsEvent.PlayPause event) throws RemoteException {
        if (mIsMusicPlayerServiceBound) {
            mMusicPlayerService.toggle();
        }
    }


    public void onEventMainThread(PlayerControlsEvent.Next event) throws RemoteException {
        if (mIsMusicPlayerServiceBound) {
            mMusicPlayerService.playNext();
        }
    }


    public void onEventMainThread(PlayerControlsEvent.Previous event) throws RemoteException {
        if (mIsMusicPlayerServiceBound) {
            mMusicPlayerService.playPrevious();
        }
    }


    public void onEventMainThread(PlayerControlsEvent.ProgressChanged event) throws RemoteException {
        if (mIsMusicPlayerServiceBound) {
            mIsSeeking = false;
            mMusicPlayerService.seekTo(event.getProgress());
        }
    }


    public void onEventMainThread(PlayerControlsEvent.SeekBarTrackingStarted event) throws RemoteException {
        mIsSeeking = true;
    }


    public void onEventMainThread(MusicPlayerServiceEvent.PlaybackStarted event) {
        mView.showPause();

    }


    public void onEventMainThread(MusicPlayerServiceEvent.PlaybackPaused event) {
        mView.showPlay();
    }


    public void onEventMainThread(MusicPlayerServiceEvent.PlaybackProgress event) {
        if (!mIsSeeking) {
            mView.setProgress((int) event.getCurrentProgress(), (int) event.getMaxProgress());
        }
    }


    private void handleOnCreateIntent() {
        mMusicFilesList = null;
        if (isExplicitIntentOpenMusicFile()) {
            mMusicFilesList = getIntent().getParcelableArrayListExtra(IntentExtra.INTENT_EXTRA_MUSIC_FILES);
        } else
            if (isImplicitIntentOpenMusicFile()) {
                mMusicFilesList = new ArrayList<MusicFile>();
                mMusicFilesList.add(new MusicFile(getIntent().getData()));
            }

    }


    private boolean isExplicitIntentOpenMusicFile() {
        return getIntent().getAction().equals(IntentAction.INTENT_ACTION_PLAY_MUSIC_FILE);
    }


    private boolean isImplicitIntentOpenMusicFile() {
        return getIntent().getAction().equals(ACTION_VIEW);
    }


    private void stopMusicPlayerService() {
        Intent stopServiceIntent = new Intent(this, MusicPlayerService.class);
        stopService(stopServiceIntent);
    }


    private void startFloatingControlsService() {
        Intent startServiceIntent = new Intent(this, FloatingControlsService.class);
        startService(startServiceIntent);
    }


    private void stopFloatingControlsService() {
        Intent stopServiceIntent = new Intent(this, FloatingControlsService.class);
        stopService(stopServiceIntent);
    }


    private void bindToMusicPlayerService() {
        Intent startServiceIntent = new Intent(this, MusicPlayerService.class);
        startService(startServiceIntent);

        bindService(startServiceIntent, mMusicPlayerServiceConnection, 0);
    }

    private class MusicPlayerServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mMusicPlayerService = IMusicPlayer.Stub.asInterface(service);
            mIsMusicPlayerServiceBound = true;
            try {
                //TODO: provide better check for when the activity is started just to show
                // the currently playing song/s instead of restarting the service with a new set of songs
                if (mMusicFilesList != null && !mMusicFilesList.isEmpty()) {
                    mMusicPlayerService.setPlayList(mMusicFilesList);
                    mMusicPlayerService.play();
                } else {
                    //TODO: Show the currently playing song info
                }
            } catch (RemoteException e) {
                //TODO: Handle exception
            }

        }


        @Override
        public void onServiceDisconnected(ComponentName name) {
            mIsMusicPlayerServiceBound = false;
        }
    }
}