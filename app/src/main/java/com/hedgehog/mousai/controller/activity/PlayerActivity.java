package com.hedgehog.mousai.controller.activity;


import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.hedgehog.mousai.IMusicPlayer;
import com.hedgehog.mousai.R;
import com.hedgehog.mousai.application.MousaiApplication;
import com.hedgehog.mousai.constant.IntentAction;
import com.hedgehog.mousai.controller.service.FloatingControlsService;
import com.hedgehog.mousai.controller.service.MusicPlayerService;
import com.hedgehog.mousai.dal.MusicFilesDao;
import com.hedgehog.mousai.event.MusicPlayerEvent;
import com.hedgehog.mousai.event.controller.service.MusicPlayerServiceEvent;
import com.hedgehog.mousai.event.theme.ThemeEvent;
import com.hedgehog.mousai.event.view.PlayerControlsEvent;
import com.hedgehog.mousai.model.music.MusicFile;
import com.hedgehog.mousai.util.ArtStore;
import com.hedgehog.mousai.util.SystemServicesUtils;
import com.hedgehog.mousai.view.PlayerView;

import java.util.ArrayList;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;

import static android.content.Intent.ACTION_VIEW;


public class PlayerActivity extends AppCompatActivity {

    private static final String TAG = PlayerActivity.class.getCanonicalName();

    @Inject
    EventBus mEventBus;

    @Inject
    MusicFilesDao mMusicFilesDao;

    private IMusicPlayer mMusicPlayerService;
    private boolean mIsMusicPlayerServiceBound;
    private ServiceConnection mMusicPlayerServiceConnection;
    private boolean mIsSeeking;
    private PlayerView mView;
    private boolean mShouldRestartPlaying;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        MousaiApplication.getControllerComponent().inject(this);
        mEventBus.register(this);
        mView = (PlayerView) findViewById(R.id.player_view);
        initSupportActionBar();

        mIsMusicPlayerServiceBound = false;
        mMusicPlayerServiceConnection = new MusicPlayerServiceConnection();

        handleOnCreateIntent();
        stopFloatingControlsService();
    }


    @Override
    protected void onResume() {
        super.onResume();
        bindToMusicPlayerService();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mIsMusicPlayerServiceBound) {
            unbindService(mMusicPlayerServiceConnection);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mEventBus.unregister(this);
        //TODO: start the floating controls when most appropriate
        // here it is started also in case of locking the screen while in this activity
        startFloatingControlsService();
    }


    public void onEventMainThread(PlayerControlsEvent.PlayPause event) throws RemoteException {
        if (mIsMusicPlayerServiceBound) {
            mMusicPlayerService.toggle();
        }
    }


    public void onEventMainThread(PlayerControlsEvent.Next event) throws RemoteException {
        if (mIsMusicPlayerServiceBound) {
            mMusicPlayerService.playNext();
        }
    }


    public void onEventMainThread(PlayerControlsEvent.Previous event) throws RemoteException {
        if (mIsMusicPlayerServiceBound) {
            mMusicPlayerService.playPrevious();
        }
    }


    public void onEventMainThread(PlayerControlsEvent.ProgressChanged event) throws RemoteException {
        if (mIsMusicPlayerServiceBound) {
            mIsSeeking = false;
            mMusicPlayerService.seekTo(event.getProgress());
        }
    }


    public void onEventMainThread(PlayerControlsEvent.SeekBarTrackingStarted event) throws RemoteException {
        mIsSeeking = true;
    }


    public void onEventMainThread(MusicPlayerEvent.PlaybackStarted event) {
        mView.showPause();
    }


    public void onEventMainThread(MusicPlayerServiceEvent.MusicFileChanged event) {
        if (mIsMusicPlayerServiceBound) {
            updateMusicFileInfo(event.musicFile);
        }
    }


    public void onEventMainThread(ThemeEvent.ThemeChanged event) {
        mView.setTheme(event.newTheme);
        SystemServicesUtils.setStatusBarColor(this, event.newTheme.getSecondaryColor());
    }


    public void onEventMainThread(MusicPlayerEvent.PlaybackPaused event) {
        mView.showPlay();
    }


    public void onEventMainThread(MusicPlayerServiceEvent.PlaybackProgress event) {
        if (!mIsSeeking) {
            mView.setProgress((int) event.getCurrentProgress(), (int) event.getMaxProgress());
        }
    }


    private void handleOnCreateIntent() {
        mShouldRestartPlaying = false;
        if (isImplicitIntentOpenMusicFile()) {
            ArrayList<MusicFile> musicFilesList = new ArrayList<MusicFile>();
            musicFilesList.add(new MusicFile(getIntent().getData()));
            mMusicFilesDao.setCurrentMusicFilesList(musicFilesList);
            mShouldRestartPlaying = true;
        } else if (isPlayMusicFileIntent()) {
            mShouldRestartPlaying = true;
        }
    }


    private boolean isResumeIntent() {
        return getIntent().getAction().equals(IntentAction.INTENT_ACTION_RESUME);
    }


    private boolean isPlayMusicFileIntent() {
        return getIntent().getAction().equals(IntentAction.INTENT_ACTION_PLAY_MUSIC_FILE);
    }

    private void initSupportActionBar() {
        setSupportActionBar(mView.getToolbar());
    }


    private void updateSupportActionBar(MusicFile musicFile) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(musicFile.getTitle());
            actionBar.setSubtitle(musicFile.getArtist());
        }
    }


    private boolean isImplicitIntentOpenMusicFile() {
        return getIntent().getAction().equals(ACTION_VIEW);
    }


    private void stopMusicPlayerService() {
        Intent stopServiceIntent = new Intent(this, MusicPlayerService.class);
        stopService(stopServiceIntent);
    }


    private void startFloatingControlsService() {
        Intent startServiceIntent = new Intent(this, FloatingControlsService.class);
        startService(startServiceIntent);
    }


    private void stopFloatingControlsService() {
        Intent stopServiceIntent = new Intent(this, FloatingControlsService.class);
        stopService(stopServiceIntent);
    }


    private void initView() throws RemoteException {
        if (mMusicPlayerService.isPlaying()) {
            mView.showPause();
        } else {
            mView.showPlay();
        }

        mView.setProgress((int) mMusicPlayerService.getProgress(), (int) mMusicPlayerService.getMaxProgress());
        updateMusicFileInfo(mMusicFilesDao.getCurrentMusicFile());
    }


    private void updateMusicFileInfo(MusicFile musicFile) {
        Bitmap albumArt = ArtStore.getAlbumArt(musicFile);
        if (albumArt != null) {
            mView.setBackground(new BitmapDrawable(getResources(), albumArt));
        } else {
            mView.setBackgroundResource(R.drawable.splash_background);
        }
        updateSupportActionBar(musicFile);
    }


    private void bindToMusicPlayerService() {
        Intent startServiceIntent = new Intent(this, MusicPlayerService.class);
        //The service may be already started.
        if (!SystemServicesUtils.isServiceRunning(getApplicationContext(), MusicPlayerService.class)) {
            startService(startServiceIntent);
        }

        bindService(startServiceIntent, mMusicPlayerServiceConnection, BIND_WAIVE_PRIORITY);
    }

    private class MusicPlayerServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mMusicPlayerService = IMusicPlayer.Stub.asInterface(service);
            mIsMusicPlayerServiceBound = true;
            try {
                if (mShouldRestartPlaying) {
                    mMusicPlayerService.play();
                }
                initView();

            } catch (RemoteException e) {
                //TODO: Handle exception
            }
        }


        @Override
        public void onServiceDisconnected(ComponentName name) {
            mIsMusicPlayerServiceBound = false;
        }
    }
}