package com.hedgehog.mousai.model.music;


import java.util.ArrayList;


public class MusicCollection extends ArrayList<MusicFile> {

    private Type mType;

    public MusicCollection(Type type) {
        mType = type;
    }

    public Type getType() {
        return mType;
    }

    public enum Type {
        PLAYLIST, ALBUM, COMMON_ARTIST, FOLDER;
    }
}
