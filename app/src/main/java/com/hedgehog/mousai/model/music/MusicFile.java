package com.hedgehog.mousai.model.music;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.io.Serializable;


/**
 * Representation of a music file that is stored in the device.
 */
public class MusicFile implements Parcelable, Serializable {

    private static final String PATH_KEY = "path";
    private static final String FILE_NAME_KEY = "file_name";
    private static final String TITLE_KEY = "title";
    private static final String ARTIST_KEY = "artist";
    private static final String ALBUM_ARTIST_KEY = "album_artist";
    private static final String AUTHOR_KEY = "author";
    private static final String ALBUM_KEY = "album";
    private static final String DURATION_KEY = "duration";
    private static final String ALBUM_ART_KEY = "album_art";
    private static final String GENRE_KEY = "genre";
    private static final String MIME_TYPE_KEY = "mime_type";

    private String mPath;
    private String mFileName;
    private String mTitle;
    private String mArtist;
    private String mAlbumArtist;
    private String mAuthor;
    private String mAlbum;
    private String mGenre;
    private String mMimeType;
    private long mDuration;
    private byte[] mAlbumArt;

    /**
     * CREATOR for a {@link MusicFile} according to the {@link Parcelable} interface.
     *
     * @see android.os.Parcelable.Creator
     */
    public static final Parcelable.Creator<MusicFile> CREATOR = new Creator<MusicFile>() {

        /**
         * {@inheritDoc}
         */
        @Override
        public MusicFile createFromParcel(Parcel source) {
            Bundle bundle = source.readBundle();
            MusicFile file = new MusicFile();

            file.mPath = bundle.getString(PATH_KEY);
            file.mFileName = bundle.getString(FILE_NAME_KEY);
            file.mTitle = bundle.getString(TITLE_KEY);
            file.mArtist = bundle.getString(ARTIST_KEY);
            file.mAlbumArtist = bundle.getString(ALBUM_ARTIST_KEY);
            file.mAlbum = bundle.getString(ALBUM_KEY);
            file.mAuthor = bundle.getString(AUTHOR_KEY);
            file.mGenre = bundle.getString(GENRE_KEY);
            file.mMimeType = bundle.getString(MIME_TYPE_KEY);
            file.mDuration = bundle.getLong(DURATION_KEY);
            file.mAlbumArt = bundle.getByteArray(ALBUM_ART_KEY);

            return file;
        }


        /**
         * {@inheritDoc}
         */
        @Override
        public MusicFile[] newArray(int size) {
            return new MusicFile[size];
        }
    };


    /**
     * Creates a new empty music file.
     */
    public MusicFile() {
        mPath = "";
        mFileName = "";
        mAlbum = "";
        mArtist = "";
        mAlbumArtist = "";
        mAuthor = "";
        mTitle = "";
        mGenre = "";
        mMimeType = "";
        mDuration = 0;
        mAlbumArt = null;
    }


    public MusicFile(Uri musicFileUri) {
        this(musicFileUri.getPath());
    }


    public MusicFile(String path) {
        mPath = path;
        int index = path.lastIndexOf(File.separator);
        mFileName = path.substring(index + 1);

        MediaMetadataRetriever metadataRetriever = getMediaMetadataRetriever(mPath);
        mAlbum = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
        mAlbumArtist = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUMARTIST);
        mArtist = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        mDuration = Long.parseLong(metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
        mAuthor = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_AUTHOR);
        mGenre = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE);
        mTitle = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
        mMimeType = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_MIMETYPE);
    }


    private MediaMetadataRetriever getMediaMetadataRetriever(String pathToMusicFile) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(pathToMusicFile);
        return mediaMetadataRetriever;
    }


    /**
     * Unused
     *
     * @return constant 0
     * @see Parcelable#describeContents()
     */
    @Override
    public int describeContents() {
        return 0;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Bundle bundle = new Bundle();

        bundle.putString(PATH_KEY, mPath);
        bundle.putString(FILE_NAME_KEY, mFileName);
        bundle.putString(TITLE_KEY, mTitle);
        bundle.putString(ARTIST_KEY, mArtist);
        bundle.putString(ALBUM_ARTIST_KEY, mAlbumArtist);
        bundle.putString(AUTHOR_KEY, mAuthor);
        bundle.putString(ALBUM_KEY, mAlbum);
        bundle.putLong(DURATION_KEY, mDuration);
        bundle.putString(GENRE_KEY, mGenre);
        bundle.putString(MIME_TYPE_KEY, mMimeType);
        bundle.putByteArray(ALBUM_ART_KEY, mAlbumArt);

        dest.writeBundle(bundle);
    }


    public String getPath() {
        return mPath;
    }


    public void setPath(String path) {
        mPath = path;
    }


    public String getTitle() {
        return mTitle;
    }


    public void setTitle(String title) {
        mTitle = title;
    }


    public String getArtist() {
        return mArtist;
    }


    public void setArtist(String artist) {
        mArtist = artist;
    }


    public String getAlbum() {
        return mAlbum;
    }


    public void setAlbum(String album) {
        mAlbum = album;
    }


    public long getDuration() {
        return mDuration;
    }


    public void setDuration(long duration) {
        mDuration = duration;
    }


    public Bitmap getAlbumArt() {
        if (mAlbumArt == null) {
            MediaMetadataRetriever metadataRetriever = getMediaMetadataRetriever(mPath);
            mAlbumArt = metadataRetriever.getEmbeddedPicture();
        }
        return BitmapFactory.decodeByteArray(mAlbumArt, 0, mAlbumArt.length);
    }


    public void setAlbumArt(byte[] albumArt) {
        mAlbumArt = albumArt;
    }


    public String getGenre() {
        return mGenre;
    }


    public void setGenre(String genre) {
        mGenre = genre;
    }


    public String getMimeType() {
        return mMimeType;
    }


    public void setMimeType(String mimeType) {
        mMimeType = mimeType;
    }


    public String getAlbumArtist() {
        return mAlbumArtist;
    }


    public void setAlbumArtist(String albumArtist) {
        mAlbumArtist = albumArtist;
    }


    public String getAuthor() {
        return mAuthor;
    }


    public void setAuthor(String author) {
        mAuthor = author;
    }


    public String getFileName() {
        return mFileName;
    }


    public void setFileName(String fileName) {
        mFileName = fileName;
    }


    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Id = ");
        stringBuilder.append("\nPath = ");
        stringBuilder.append(getPath());
        stringBuilder.append("\nFile name = ");
        stringBuilder.append(getFileName());
        stringBuilder.append("\nAlbumName = ");
        stringBuilder.append(getAlbum());
        stringBuilder.append("\nAlbum Artist = ");
        stringBuilder.append(getAlbumArtist());
        stringBuilder.append("\nTitle = ");
        stringBuilder.append(getTitle());
        stringBuilder.append("\nArtist = ");
        stringBuilder.append(getArtist());
        stringBuilder.append("\nGenre = ");
        stringBuilder.append(getGenre());
        stringBuilder.append("\nAuthor = ");
        stringBuilder.append(getAuthor());
        stringBuilder.append("\nMime type = ");
        stringBuilder.append(getMimeType());
        stringBuilder.append("\nDuration = ");
        stringBuilder.append(getDuration());

        return stringBuilder.toString();
    }
}
