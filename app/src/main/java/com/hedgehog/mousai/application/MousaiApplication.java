package com.hedgehog.mousai.application;


import android.app.Application;

import com.hedgehog.mousai.di.component.ControllerComponent;
import com.hedgehog.mousai.di.component.DaggerControllerComponent;
import com.hedgehog.mousai.di.component.DaggerMusicPlayerComponent;
import com.hedgehog.mousai.di.component.DaggerViewComponent;
import com.hedgehog.mousai.di.component.MusicPlayerComponent;
import com.hedgehog.mousai.di.component.ViewComponent;
import com.hedgehog.mousai.di.module.ApplicationModule;
import com.hedgehog.mousai.di.module.ThemeModule;


public class MousaiApplication extends Application {

    private static ControllerComponent sControllerComponent;

    private static ViewComponent sViewComponent;

    private static MusicPlayerComponent sMusicPlayerComponent;


    public static ControllerComponent getControllerComponent() {
        return sControllerComponent;
    }


    public static ViewComponent getViewComponent() {
        return sViewComponent;
    }


    public static MusicPlayerComponent getsMusicPlayerComponent() {
        return sMusicPlayerComponent;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        initControllerComponent();
    }


    private void initControllerComponent() {
        ApplicationModule applicationModule = new ApplicationModule(this);
        sControllerComponent = DaggerControllerComponent.builder().applicationModule(applicationModule).build();
        sViewComponent = DaggerViewComponent.builder().applicationModule(applicationModule)
                .themeModule(new ThemeModule(applicationModule.provideEventBus())).build();

        sMusicPlayerComponent = DaggerMusicPlayerComponent.builder().applicationModule(applicationModule).build();

    }

}
