package com.hedgehog.mousai.dal;


import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import com.hedgehog.mousai.model.music.MusicFile;

import java.util.ArrayList;
import java.util.List;


/**
 * DAO for accessing all music files that are going to be played by the application
 */
public class MusicFilesDao {

    private Context              mContext;
    private ArrayList<MusicFile> mMusicFilesList;


    public MusicFilesDao(Context context) {
        mContext = context;
        getAllMusicFilesFromStorage();
    }


    public List<MusicFile> getMusicFilesList() {
        return mMusicFilesList;
    }


    private void getAllMusicFilesFromStorage() {
        mMusicFilesList = new ArrayList<MusicFile>();
        ContentResolver resolver = mContext.getContentResolver();

        Cursor cursor = resolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null,
                MediaStore.Audio.Media.TITLE);

        if (cursor != null) {
            // cursor could be null when no external sd card is found (for
            // example when the device is connected to a pc as a storage)
            MusicFile currentMusicFile;
            while (cursor.moveToNext()) {
                currentMusicFile = parseMusicFileRow(cursor);
                mMusicFilesList.add(currentMusicFile);
            }
            cursor.close();
        }
    }


    private MusicFile parseMusicFileRow(Cursor cursor) {
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
        MusicFile musicFile = new MusicFile(path);
        return musicFile;
    }
}