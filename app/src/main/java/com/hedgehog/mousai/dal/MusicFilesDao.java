package com.hedgehog.mousai.dal;


import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import com.hedgehog.mousai.model.music.MusicFile;
import com.hedgehog.mousai.util.CollectionUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


/**
 * DAO for accessing all music files that are going to be played by the application
 */
public class MusicFilesDao {

    private static final String CACHE_CURRENT_MUSIC_FILE_FILE_NAME       = "CACHE_CURRENT_MUSIC_FILE_FILE_NAME";
    private static final String CACHE_CURRENT_MUSIC_FILES_LIST_FILE_NAME = "CACHE_CURRENT_MUSIC_FILES_LIST_FILE_NAME";
    private Context              mContext;
    private MusicFile            mCurrentMusicFile;
    private int                  mCurrentMusicFileIndex;
    private ArrayList<MusicFile> mCurrentMusicFilesList;
    private ArrayList<MusicFile> mAllMusicFilesList;


    @Inject
    public MusicFilesDao(Context context) {
        mContext = context;
        mCurrentMusicFilesList = getCurrentMusicFilesListFromCache();
    }


    /**
     * @return The current {@link MusicFile} or <code>null</code> if there is no current music files list set.
     */
    public MusicFile getCurrentMusicFile() {
        if (mCurrentMusicFile == null) {
            mCurrentMusicFile = getCurrentMusicFileFromCache();
            //if there was no cached current music file, select the first music file from the current music files list.
            if (mCurrentMusicFile == null && !CollectionUtils.isNullOrEmpty(mCurrentMusicFilesList)) {
                mCurrentMusicFile = mCurrentMusicFilesList.get(0);
            }
        }

        return mCurrentMusicFile;
    }


    public void setCurrentMusicFile(MusicFile musicFile) {
        if (musicFile != null) {
            // Make sure the current music files list is properly set
            if (CollectionUtils.isNullOrEmpty(mCurrentMusicFilesList) || !mCurrentMusicFilesList.contains(musicFile)) {
                mCurrentMusicFilesList = new ArrayList<>();
                mCurrentMusicFilesList.add(musicFile);
            }

            mCurrentMusicFile = musicFile;
            mCurrentMusicFileIndex = mCurrentMusicFilesList.indexOf(mCurrentMusicFile);
            cacheCurrentMusicFile();
        }
    }


    public List<MusicFile> getAllMusicFilesList() {
        if (CollectionUtils.isNullOrEmpty(mAllMusicFilesList)) {
            loadAllMusicFilesFromStorage();
        }
        return mAllMusicFilesList;
    }


    public ArrayList<MusicFile> getCurrentMusicFilesList() {
        if (CollectionUtils.isNullOrEmpty(mCurrentMusicFilesList)) {
            loadAllMusicFilesFromStorage();
            mCurrentMusicFilesList = mAllMusicFilesList;
            cacheCurrentMusicFilesList();
        }
        return mCurrentMusicFilesList;
    }


    public void setCurrentMusicFilesList(ArrayList<MusicFile> musicFilesList) {
        mCurrentMusicFilesList = musicFilesList;
        cacheCurrentMusicFilesList();
    }


    public int getCurrentMusicFileIndex() {
        return mCurrentMusicFileIndex;
    }


    /**
     * Set the current music files list to be in the natural order but starting from specific {@link MusicFile}
     *
     * @param musicFile
     */
    public void setStartingMusicFile(MusicFile musicFile) {
        mCurrentMusicFilesList = reorderAllMusicFilesList(musicFile);
    }


    private void cacheCurrentMusicFile() {
        if (mCurrentMusicFile != null) {
            FileOutputStream outputStream;
            ObjectOutputStream objectOutputStream;
            try {
                outputStream = mContext.openFileOutput(CACHE_CURRENT_MUSIC_FILE_FILE_NAME, Context.MODE_PRIVATE);
                objectOutputStream = new ObjectOutputStream(outputStream);
                objectOutputStream.writeObject(mCurrentMusicFile);
                objectOutputStream.close();
                outputStream.close();
            } catch (FileNotFoundException e) {
                //Nothing to do
            } catch (IOException e) {
                //Nothing to do
            }
        }
    }


    private MusicFile getCurrentMusicFileFromCache() {
        FileInputStream inputStream;
        ObjectInputStream objectInputStream;
        try {
            inputStream = mContext.openFileInput(CACHE_CURRENT_MUSIC_FILE_FILE_NAME);
            objectInputStream = new ObjectInputStream(inputStream);
            MusicFile musicFile = (MusicFile) objectInputStream.readObject();
            objectInputStream.close();
            inputStream.close();
            return musicFile;
        } catch (FileNotFoundException e) {
            //Nothing to do
        } catch (IOException e) {
            //Nothing to do
        } catch (ClassNotFoundException e) {
            //Nothing to do
        }
        return null;
    }


    private void cacheCurrentMusicFilesList() {
        if (!CollectionUtils.isNullOrEmpty(mCurrentMusicFilesList)) {
            FileOutputStream outputStream;
            ObjectOutputStream objectOutputStream;
            try {
                outputStream = mContext.openFileOutput(CACHE_CURRENT_MUSIC_FILES_LIST_FILE_NAME, Context.MODE_PRIVATE);
                objectOutputStream = new ObjectOutputStream(outputStream);
                objectOutputStream.writeObject(mCurrentMusicFilesList);
                objectOutputStream.close();
                outputStream.close();
            } catch (FileNotFoundException e) {
                //Nothing to do
            } catch (IOException e) {
                //Nothing to do
            }
        }
    }


    private ArrayList<MusicFile> getCurrentMusicFilesListFromCache() {
        FileInputStream inputStream;
        ObjectInputStream objectInputStream;
        try {
            inputStream = mContext.openFileInput(CACHE_CURRENT_MUSIC_FILES_LIST_FILE_NAME);
            objectInputStream = new ObjectInputStream(inputStream);
            ArrayList<MusicFile> musicFiles = (ArrayList<MusicFile>) objectInputStream.readObject();
            objectInputStream.close();
            inputStream.close();
            return musicFiles;
        } catch (FileNotFoundException e) {
            //Nothing to do
        } catch (IOException e) {
            //Nothing to do
        } catch (ClassNotFoundException e) {
            //Nothing to do
        }
        return null;
    }


    private void loadAllMusicFilesFromStorage() {
        mAllMusicFilesList = new ArrayList<>();
        ContentResolver resolver = mContext.getContentResolver();

        Cursor cursor = resolver
                .query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null, MediaStore.Audio.Media.TITLE);

        if (cursor != null) {
            // cursor could be null when no external sd card is found (for
            // example when the device is connected to a pc as a storage)
            MusicFile currentMusicFile;
            while (cursor.moveToNext()) {
                currentMusicFile = parseMusicFileRow(cursor);
                mAllMusicFilesList.add(currentMusicFile);
            }
            cursor.close();
        }
    }


    private MusicFile parseMusicFileRow(Cursor cursor) {
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
        return new MusicFile(path);
    }


    private ArrayList<MusicFile> reorderAllMusicFilesList(MusicFile musicFile) {
        setCurrentMusicFile(musicFile);
        ArrayList<MusicFile> playList = new ArrayList<>();
        int startIndex = mAllMusicFilesList.indexOf(musicFile);
        int currentIndex = startIndex;
        do {
            playList.add(mAllMusicFilesList.get(currentIndex));
            currentIndex = (currentIndex + 1) % mAllMusicFilesList.size();
        } while (currentIndex != startIndex);

        return playList;
    }
}