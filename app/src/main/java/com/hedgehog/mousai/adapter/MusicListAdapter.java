package com.hedgehog.mousai.adapter;


import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hedgehog.mousai.R;
import com.hedgehog.mousai.model.music.MusicFile;
import com.hedgehog.mousai.view.ImageLoader;


/**
 * Adapter used to build the views for music files in music list view
 */
public class MusicListAdapter extends BaseAdapter {

    private List<MusicFile>        mMusicFilesList;
    private Context                mContext;
    private ImageLoader<MusicFile> mImageLoader;


    public MusicListAdapter(Context context, List<MusicFile> musicFilesList) {
        mContext = context;
        mMusicFilesList = musicFilesList;
    }


    public MusicListAdapter(Context context, List<MusicFile> musicFilesList, ImageLoader<MusicFile> imageLoader) {
        this(context, musicFilesList);
        mImageLoader = imageLoader;
    }


    public void setImageLoader(ImageLoader<MusicFile> loader) {
        mImageLoader = loader;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return mMusicFilesList.size();
    }


    @Override
    public MusicFile getItem(int position) {
        return mMusicFilesList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.music_list_item, parent, false);
            holder = new ViewHolder();
            holder.musicTitleTextView = (TextView) convertView.findViewById(R.id.music_title_id);
            holder.albumArtImageView = (ImageView) convertView.findViewById(R.id.music_list_item_art);
            convertView.setTag(holder);
        }

        MusicFile musicFile = mMusicFilesList.get(position);
        holder = (ViewHolder) convertView.getTag();
        holder.musicTitleTextView.setText(musicFile.getFileName());

        if (mImageLoader != null) {
            mImageLoader.load(musicFile, holder.albumArtImageView);
        }

        return convertView;
    }

    private class ViewHolder {

        TextView  musicTitleTextView;
        ImageView albumArtImageView;
    }
}