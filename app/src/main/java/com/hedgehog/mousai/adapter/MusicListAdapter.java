package com.hedgehog.mousai.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hedgehog.mousai.R;
import com.hedgehog.mousai.model.music.MusicFile;

import java.util.List;


/**
 * Adapter used to build the views for music files in music list view
 */
public class MusicListAdapter extends BaseAdapter {

    private List<MusicFile> mMusicFilesList;
    private Context         mContext;


    public MusicListAdapter(Context context, List<MusicFile> musicFilesList) {
        mContext = context;
        mMusicFilesList = musicFilesList;
    }


    @Override
    public int getCount() {
        return mMusicFilesList.size();
    }


    @Override
    public MusicFile getItem(int position) {
        return mMusicFilesList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.music_list_item, parent, false);
            holder = new ViewHolder();
            holder.musicTitleTextView = (TextView) convertView.findViewById(R.id.music_title_id);
            convertView.setTag(holder);
        }

        holder = (ViewHolder) convertView.getTag();
        holder.musicTitleTextView.setText(getMusicFileName(position));

        return convertView;
    }


    private String getMusicFileName(int position) {
        MusicFile musicFile = mMusicFilesList.get(position);
        String musicFileName = musicFile.getFileName();

        return musicFileName;
    }

    private class ViewHolder {

        TextView musicTitleTextView;
    }
}