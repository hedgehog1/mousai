package com.hedgehog.mousai.async;


import com.android.util.AsyncTask;
import com.hedgehog.mousai.application.MousaiApplication;
import com.hedgehog.mousai.dal.MusicFilesDao;
import com.hedgehog.mousai.model.music.MusicFile;

import javax.inject.Inject;
import java.util.List;


public class GetAllMusicFilesTask extends AsyncTask<Void, Void, List<MusicFile>> {

    @Inject
    MusicFilesDao mMusicFilesDao;


    public GetAllMusicFilesTask() {
        MousaiApplication.getControllerComponent().inject(this);
    }


    @Override
    protected List<MusicFile> doInBackground(Void... params) {
        return mMusicFilesDao.getAllMusicFilesList();
    }

}
