package com.hedgehog.mousai.event.view;


import com.hedgehog.mousai.event.Event;


/**
 * Base event that is posted as a result of a click.
 */
public class ClickEvent extends Event {

}
