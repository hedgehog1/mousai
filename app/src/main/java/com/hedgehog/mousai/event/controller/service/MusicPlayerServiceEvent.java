package com.hedgehog.mousai.event.controller.service;


import com.hedgehog.mousai.event.Event;


/**
 * Contains all events that are triggered within {@link com.hedgehog.mousai.controller.service.MusicPlayerService
 * MusicPlayerServiceEvent}.
 */
public class MusicPlayerServiceEvent {

    /**
     * Event for when the playback in the background service has started.
     */
    public static class PlaybackStarted extends Event {

    }

    public static class PlaybackProgress extends Event {

        private long mMaxProgress;
        private long mCurrentProgress;


        public PlaybackProgress(long currentProgress, long maxProgress) {
            mCurrentProgress = currentProgress;
            mMaxProgress = maxProgress;
        }


        public long getMaxProgress() {
            return mMaxProgress;
        }


        public long getCurrentProgress() {
            return mCurrentProgress;
        }
    }

    /**
     * Event for when the playback in the background service has paused.
     */
    public static class PlaybackPaused extends Event {

    }

    /**
     * Event for when playback in the background service has stopped along with the service.
     */
    public static class ServiceStopped extends Event {

    }

}
