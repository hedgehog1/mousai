package com.hedgehog.mousai.event.controller.service;


import com.hedgehog.mousai.event.Event;
import com.hedgehog.mousai.model.music.MusicFile;


/**
 * Contains all events that are triggered within {@link com.hedgehog.mousai.controller.service.MusicPlayerService
 * MusicPlayerServiceEvent}.
 */
public class MusicPlayerServiceEvent {

    public static class PlaybackProgress extends Event {

        private long mMaxProgress;
        private long mCurrentProgress;


        public PlaybackProgress(long currentProgress, long maxProgress) {
            mCurrentProgress = currentProgress;
            mMaxProgress = maxProgress;
        }


        public long getMaxProgress() {
            return mMaxProgress;
        }


        public long getCurrentProgress() {
            return mCurrentProgress;
        }
    }

    /**
     * Event for when the current song has changed.
     */
    public static class MusicFileChanged extends Event {

        public MusicFile musicFile;


        public MusicFileChanged(MusicFile musicFile) {
            this.musicFile = musicFile;
        }
    }

}
