package com.hedgehog.mousai.event.theme;


import com.hedgehog.mousai.theme.Theme;


public class ThemeEvent {

    public static class ThemeChanged {

        public Theme newTheme;


        public ThemeChanged(Theme newTheme) {
            this.newTheme = newTheme;
        }
    }

}
