package com.hedgehog.mousai.event.view;


/**
 * Contains all events that are triggered within {@link com.hedgehog.mousai.view.FloatingControlsView
 * FloatingControlsView}.
 */
public class FloatingControlsEvent {

    /**
     * Posted when the "Play/Pause" button is clicked.
     */
    public static class PlayPause extends ClickEvent {

    }
}
