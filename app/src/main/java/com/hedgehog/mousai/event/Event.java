package com.hedgehog.mousai.event;


import java.io.Serializable;


/**
 * Base class for all events used in the EventBus.
 */
public class Event implements Serializable {

}