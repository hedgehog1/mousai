package com.hedgehog.mousai.event.view;


/**
 * Contains all events that are triggered within {@link com.hedgehog.mousai.view.MusicNotificationRemoteView
 * MusicNotificationRemoteView}.
 */
public class MusicNotificationEvent {

    /**
     * Posted when the "Play/Pause" button is clicked.
     */
    public static class PlayPause extends ClickEvent {

    }

    /**
     * Posted when the "Next" button is clicked.
     */
    public static class Next extends ClickEvent {

    }

    /**
     * Posted when the "Previous" button is clicked.
     */
    public static class Previous extends ClickEvent {

    }

    /**
     * Posted when the "Close" button is clicked.
     */
    public static class Close extends ClickEvent {

    }
}
