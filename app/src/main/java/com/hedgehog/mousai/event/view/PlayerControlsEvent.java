package com.hedgehog.mousai.event.view;


/**
 * Contains all events that are triggered within {@link com.hedgehog.mousai.view.PlayerView PlayerView}.
 */
public class PlayerControlsEvent {

    /**
     * Posted when the "Play/Pause" button is clicked.
     */
    public static class PlayPause extends ClickEvent {

    }

    /**
     * Posted when the "Next" button is clicked.
     */
    public static class Next extends ClickEvent {

    }

    /**
     * Posted when the "Previous" button is clicked.
     */
    public static class Previous extends ClickEvent {

    }

    public static class ProgressChanged extends ClickEvent {

        private int mProgress;


        public ProgressChanged(int progress) {
            mProgress = progress;
        }


        public int getProgress() {
            return mProgress;
        }
    }

    public static class SeekBarTrackingStarted extends ClickEvent {

    }
}
