package com.hedgehog.mousai.event.view;


import com.hedgehog.mousai.model.music.MusicFile;


/**
 * Contains all events that are triggered within {@link com.hedgehog.mousai.view.MusicListView MusicListView}.
 */
public class MusicListEvent {

    /**
     * This event is triggered when a music file is selected from the list. The event object carries the MusicFile that
     * has been chosen.
     */
    public static class MusicFileSelected extends ClickEvent {

        public MusicFile musicFile;


        public MusicFileSelected(MusicFile musicFile) {
            this.musicFile = musicFile;
        }
    }

}
