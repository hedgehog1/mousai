package com.hedgehog.mousai.event;


public class MusicPlayerEvent extends Event {

    /**
     * Event for when the playback in the background service has started.
     */
    public static class PlaybackStarted extends Event {

    }

    /**
     * Event for when the playback in the background service has paused.
     */
    public static class PlaybackPaused extends Event {

    }

    /**
     * Event for when the playback of a music file is completed
     */
    public static class PlaybackCompleted extends Event {

    }
}
