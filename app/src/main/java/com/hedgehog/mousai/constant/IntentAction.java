package com.hedgehog.mousai.constant;


/**
 * Class containing constants for custom Intent actions
 */
public class IntentAction {

    public static final String INTENT_ACTION_PLAY_MUSIC_FILE = "com.hedgehog.mousai.INTENT_ACTION_PLAY_MUSIC_FILE";

    public static final String INTENT_ACTION_PROXY_TO_EVENT_BUS = "com.hedgehog.mousai.INTENT_ACTION_PROXY_TO_EVENT_BUS";

    public static final String INTENT_ACTION_RESUME = "com.hedgehog.mousai.INTENT_ACTION_RESUME";

    public static final String INTENT_ACTION_BROADCAST_MUSIC_PLAYER_STARTED = "com.hedgehog.mousai.INTENT_ACTION_BROADCAST_MUSIC_PLAYER_STARTED";
}
