package com.hedgehog.mousai.constant;


/**
 * Class containing constants for request code that are being sent in application's Intents.
 */
public class IntentRequestCode {

    public static final int CODE_NOTIFICATION_PLAY_PAUSE = 0;
    public static final int CODE_NOTIFICATION_NEXT       = 1;
    public static final int CODE_NOTIFICATION_PREVIOUS   = 2;
    public static final int CODE_NOTIFICATION_CLOSE      = 3;
}
