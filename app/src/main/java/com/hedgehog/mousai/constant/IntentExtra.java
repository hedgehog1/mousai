package com.hedgehog.mousai.constant;


/**
 * Class containing constants for extras that are being sent in application's Intents
 */
public class IntentExtra {

    public static final String INTENT_EXTRA_EVENT_OBJECT = "com.hedgehog.mousai.INTENT_EXTRA_EVENT_OBJECT";
}
