package com.hedgehog.mousai.musicplayer;


/**
 * Class holding all various states that Android's MediaPlayer could reside in
 */
public class MediaPlayerStates {

    public static final int IDLE               = 1;
    public static final int INITIALIZED        = 2;
    public static final int PREPARING          = 3;
    public static final int PREPARED           = 4;
    public static final int STARTED            = 5;
    public static final int PAUSED             = 6;
    public static final int STOPPED            = 7;
    public static final int PLAYBACK_COMPLETED = 8;
    public static final int END                = 9;
    public static final int ERROR              = 10;
}
