package com.hedgehog.mousai.musicplayer;


import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.os.PowerManager;
import android.util.Log;

import com.hedgehog.mousai.controller.service.MusicPlayerService;
import com.hedgehog.mousai.model.music.MusicFile;

import java.io.IOException;


/**
 * A wrapper class of Android's MediaPlayer class, handling the MediaPlayer states and playback control functions. This
 * class plays MusicFiles.
 */
public class MusicPlayer {

    private static final String TAG = MusicPlayerService.class.getCanonicalName();

    private Context     mContext;
    private MusicFile   mCurrentMusicFile;
    private MediaPlayer mMediaPlayer;
    private int         mMediaPlayerState;


    public MusicPlayer(Context context) {
        mContext = context;
    }


    public MusicPlayer(MusicFile musicFile, Context context) {
        this(context);
        setMusicFile(musicFile);
    }


    /**
     * Set the {@link MusicFile music file} that the {@link MusicPlayer} will operate on.
     * 
     * @param musicFile
     *            the music file that the player should operate on
     * @return <code>true</code> if the musicFile was set successfully and could be operated on or <code>false</code>
     *         otherwise.
     */
    public boolean setMusicFile(MusicFile musicFile) {
        mCurrentMusicFile = musicFile;
        try {
            initOrReset();
            boolean isDataSourceSet = setDataSource(mCurrentMusicFile.getPath());
            if (isDataSourceSet) {
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            /**
             * IOException is thrown if there is some error in the music file path. Noting to do here.
             */
            Log.e(TAG, "init() - Could not set data source");
            e.printStackTrace();
        }
        return false;
    }


    /**
     * Start playing the predefined {@link MusicFile music file}.
     * 
     * @return <code>true</code> if the player was started and <code>false</code> otherwise.
     */
    public boolean play() {
        if (canBeStarted()) {
            mMediaPlayer.start();
            mMediaPlayerState = MediaPlayerStates.STARTED;
            return true;
        } else {
            try {
                boolean shouldRetryToPlay = prepare();
                if (shouldRetryToPlay) {
                    return play();
                }
            } catch (IOException e) {
                /**
                 * IOException is thrown if the player could not be prepared. Noting to do here.
                 */
                Log.e(TAG, "play() - Could not prepare the player");
                e.printStackTrace();
            }
        }
        return false;
    }


    /**
     * Stop playing the predefined {@link MusicFile music file}.
     * 
     * @return <code>true</code> if the player was stopped and <code>false</code> otherwise.
     */
    public boolean stop() {
        if (canBeStopped()) {
            mMediaPlayer.stop();
            mMediaPlayerState = MediaPlayerStates.STOPPED;
            return true;
        }
        return false;
    }


    /**
     * Pause playing the predefined {@link MusicFile music file}.
     * 
     * @return <code>true</code> if the player was paused and <code>false</code> otherwise.
     */
    public boolean pause() {
        if (canBePaused()) {
            mMediaPlayer.pause();
            mMediaPlayerState = MediaPlayerStates.PAUSED;
            return true;
        }
        return false;
    }


    /**
     * Release player when not needed.
     */
    public void release() {
        if (mMediaPlayer != null) {
            if (isPlaying()) {
                stop();
            }
            mMediaPlayer.release();
            mMediaPlayer = null;
            mMediaPlayerState = MediaPlayerStates.END;
        }
    }


    /**
     * Set looping on or off of the predefined {@link MusicFile music file}.
     * 
     * @return <code>true</code> if the looping was set and <code>false</code> otherwise.
     */
    public boolean setLooping(boolean looping) {
        if (canSetLooping()) {
            mMediaPlayer.setLooping(looping);
            return true;
        }
        return false;
    }


    /**
     * Check if the {@link MediaPlayer} is currently playing {@link MusicFile music file}
     *
     * @return <code>true</code> if music file is currently playing and <code>false</code> otherwise.
     */
    public boolean isPlaying() {
        if (canCheckIsPlaying()) {
            return mMediaPlayer.isPlaying();
        }
        return false;
    }


    /**
     * Get the current position of the currently playing {@link MusicFile music file}
     *
     * @return the current position in milliseconds
     */
    public long getCurrentProgress() {
        if (canGetCurrentPosition()) {
            return mMediaPlayer.getCurrentPosition();
        }

        return 0;
    }


    public void seekTo(int progress) {
        if (canSeek()) {
            mMediaPlayer.seekTo(progress);
        }
    }


    public long getMaxProgress() {
        if (mCurrentMusicFile != null) {
            return mCurrentMusicFile.getDuration();
        }
        return 0;
    }


    private boolean setDataSource(String path) throws IOException {
        if (canSetDataSource()) {
            mMediaPlayer.setDataSource(path);
            mMediaPlayerState = MediaPlayerStates.INITIALIZED;
            return true;
        }
        return false;
    }


    private boolean prepare() throws IOException {
        if (canBePrepared()) {
            mMediaPlayer.prepare();
            mMediaPlayerState = MediaPlayerStates.PREPARED;
            return true;
        }
        return false;
    }


    private void initOrReset() {
        if (canBeReset()) {
            mMediaPlayer.reset();
        } else {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setWakeMode(mContext, PowerManager.PARTIAL_WAKE_LOCK);
            mMediaPlayer.setOnCompletionListener(new MusicPlayer.MusicPlayerCompletionListener());
            mMediaPlayer.setOnErrorListener(new MusicPlayer.MusicPlayerErrorListener());
        }
        mMediaPlayerState = MediaPlayerStates.IDLE;
    }


    private boolean canSetDataSource() {
        if (mMediaPlayer != null) {
            switch (mMediaPlayerState) {
            case MediaPlayerStates.IDLE:
                return true;
            default:
                return false;
            }
        }
        return false;
    }


    private boolean canBeStarted() {
        if (mMediaPlayer != null) {
            switch (mMediaPlayerState) {
            case MediaPlayerStates.PREPARED:
            case MediaPlayerStates.STARTED:
            case MediaPlayerStates.PAUSED:
            case MediaPlayerStates.PLAYBACK_COMPLETED:
                return true;
            default:
                return false;
            }
        }
        return false;
    }


    private boolean canBeStopped() {
        if (mMediaPlayer != null) {
            switch (mMediaPlayerState) {
            case MediaPlayerStates.PREPARED:
            case MediaPlayerStates.STARTED:
            case MediaPlayerStates.STOPPED:
            case MediaPlayerStates.PAUSED:
            case MediaPlayerStates.PLAYBACK_COMPLETED:
                return true;
            default:
                return false;
            }
        }
        return false;
    }


    private boolean canBePaused() {
        if (mMediaPlayer != null) {
            switch (mMediaPlayerState) {
            case MediaPlayerStates.STARTED:
            case MediaPlayerStates.PAUSED:
            case MediaPlayerStates.PLAYBACK_COMPLETED:
                return true;
            default:
                return false;
            }
        }
        return false;
    }


    private boolean canBePrepared() {
        if (mMediaPlayer != null) {
            switch (mMediaPlayerState) {
            case MediaPlayerStates.INITIALIZED:
            case MediaPlayerStates.STOPPED:
                return true;
            default:
                return false;
            }
        }
        return false;
    }


    private boolean canBeReset() {
        if (mMediaPlayer != null) {
            switch (mMediaPlayerState) {
            case MediaPlayerStates.IDLE:
            case MediaPlayerStates.INITIALIZED:
            case MediaPlayerStates.PREPARED:
            case MediaPlayerStates.STARTED:
            case MediaPlayerStates.PAUSED:
            case MediaPlayerStates.STOPPED:
            case MediaPlayerStates.PLAYBACK_COMPLETED:
            case MediaPlayerStates.ERROR:
                return true;
            default:
                return false;
            }
        }
        return false;
    }


    private boolean canSetLooping() {
        if (mMediaPlayer != null) {
            switch (mMediaPlayerState) {
            case MediaPlayerStates.IDLE:
            case MediaPlayerStates.INITIALIZED:
            case MediaPlayerStates.STOPPED:
            case MediaPlayerStates.PREPARED:
            case MediaPlayerStates.STARTED:
            case MediaPlayerStates.PAUSED:
            case MediaPlayerStates.PLAYBACK_COMPLETED:
                return true;
            default:
                return false;
            }
        }
        return false;
    }


    private boolean canCheckIsPlaying() {
        if (mMediaPlayer != null) {
            switch (mMediaPlayerState) {
            case MediaPlayerStates.IDLE:
            case MediaPlayerStates.INITIALIZED:
            case MediaPlayerStates.PREPARED:
            case MediaPlayerStates.STARTED:
            case MediaPlayerStates.PAUSED:
            case MediaPlayerStates.STOPPED:
            case MediaPlayerStates.PLAYBACK_COMPLETED:
                return true;
            default:
                return false;
            }
        }
        return false;
    }


    private boolean canGetCurrentPosition() {
        if (mMediaPlayer != null) {
            switch (mMediaPlayerState) {
            case MediaPlayerStates.IDLE:
            case MediaPlayerStates.INITIALIZED:
            case MediaPlayerStates.PREPARED:
            case MediaPlayerStates.STARTED:
            case MediaPlayerStates.PAUSED:
            case MediaPlayerStates.STOPPED:
            case MediaPlayerStates.PLAYBACK_COMPLETED:
                return true;
            default:
                return false;
            }
        }
        return false;
    }


    private boolean canSeek() {
        if (mMediaPlayer != null) {
            switch (mMediaPlayerState) {
            case MediaPlayerStates.PREPARED:
            case MediaPlayerStates.STARTED:
            case MediaPlayerStates.PAUSED:
            case MediaPlayerStates.PLAYBACK_COMPLETED:
                return true;
            default:
                return false;
            }
        }
        return false;
    }

    private class MusicPlayerErrorListener implements OnErrorListener {

        @Override
        public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
            mMediaPlayerState = MediaPlayerStates.ERROR;
            initOrReset();
            return true;
        }
    }

    private class MusicPlayerCompletionListener implements MediaPlayer.OnCompletionListener {

        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            if (!mediaPlayer.isLooping()) {
                mMediaPlayerState = MediaPlayerStates.PLAYBACK_COMPLETED;
            }
        }
    }
}