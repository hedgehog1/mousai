package com.hedgehog.mousai.di.component;


import javax.inject.Singleton;

import com.hedgehog.mousai.async.GetAllMusicFilesTask;
import com.hedgehog.mousai.di.module.ApplicationModule;
import com.hedgehog.mousai.musicplayer.MusicPlayer;

import dagger.Component;


@Singleton
@Component(modules = { ApplicationModule.class
})
public interface MusicPlayerComponent {

    void inject(MusicPlayer musicPlayer);
}
