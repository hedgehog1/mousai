package com.hedgehog.mousai.di.module;


import android.graphics.Color;

import com.hedgehog.mousai.theme.SongArtThemeProvider;
import com.hedgehog.mousai.theme.Theme;
import com.hedgehog.mousai.theme.ThemeProvider;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;


@Module
public class ThemeModule {

    private ThemeProvider mProvider;
    private Theme         mDefaultTheme;


    public ThemeModule(EventBus eventBus) {
        mDefaultTheme = new Theme(0xff445963, 0xfffdbf07, 0xff445963, 0xfffdbf07);
        mProvider = new SongArtThemeProvider(eventBus, mDefaultTheme);
    }


    @Provides
    public Theme provideTheme() {
        return mProvider.getTheme();
    }
}
