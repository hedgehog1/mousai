package com.hedgehog.mousai.di.module;


import javax.inject.Singleton;

import android.app.Application;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.SharedPreferences;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;


@Module
public class ApplicationModule {

    private Application mApplication;


    public ApplicationModule(Application application) {
        mApplication = application;
    }


    @Singleton
    @Provides
    public Context provideContext() {
        return mApplication;
    }


    @Singleton
    @Provides
    public EventBus provideEventBus() {
        return EventBus.getDefault();
    }


    @Provides
    public SharedPreferences provideSharedPreferences() {
        return mApplication.getSharedPreferences(mApplication.getApplicationInfo().name, Context.MODE_PRIVATE);
    }


    @Provides
    public AppWidgetManager provideAppWidgetManager(Context context) {
        return AppWidgetManager.getInstance(context);
    }

}
