package com.hedgehog.mousai.di.component;


import com.hedgehog.mousai.di.module.ApplicationModule;
import com.hedgehog.mousai.di.module.ThemeModule;
import com.hedgehog.mousai.view.FloatingControlsView;
import com.hedgehog.mousai.view.MusicListView;
import com.hedgehog.mousai.view.MusicNotificationRemoteView;
import com.hedgehog.mousai.view.PlayerView;
import com.hedgehog.mousai.view.WidgetRemoteView;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {ApplicationModule.class, ThemeModule.class})
public interface ViewComponent {

    void inject(FloatingControlsView floatingControlsView);


    void inject(MusicListView musicListView);


    void inject(MusicNotificationRemoteView musicNotificationRemoteView);


    void inject(PlayerView playerView);


    void inject(WidgetRemoteView widgetRemoteView);
}
