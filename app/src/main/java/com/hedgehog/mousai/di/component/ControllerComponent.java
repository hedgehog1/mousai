package com.hedgehog.mousai.di.component;


import javax.inject.Singleton;

import com.hedgehog.mousai.async.GetAllMusicFilesTask;
import com.hedgehog.mousai.controller.activity.MusicListActivity;
import com.hedgehog.mousai.controller.activity.PlayerActivity;
import com.hedgehog.mousai.controller.notification.MusicNotification;
import com.hedgehog.mousai.controller.service.FloatingControlsService;
import com.hedgehog.mousai.controller.service.MusicPlayerService;
import com.hedgehog.mousai.controller.service.WidgetUpdateService;
import com.hedgehog.mousai.di.module.ApplicationModule;
import com.hedgehog.mousai.di.module.DalModule;

import dagger.Component;


@Singleton
@Component(modules = { ApplicationModule.class, DalModule.class
})
public interface ControllerComponent {

    void inject(MusicListActivity musicListActivity);

    void inject(PlayerActivity playerActivity);

    void inject(MusicPlayerService musicPlayerService);

    void inject(FloatingControlsService floatingControlsService);

    void inject(WidgetUpdateService widgetUpdateService);

    void inject(MusicNotification musicNotification);

    void inject(GetAllMusicFilesTask getAllMusicFilesTask);
}
