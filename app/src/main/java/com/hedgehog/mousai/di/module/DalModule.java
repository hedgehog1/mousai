package com.hedgehog.mousai.di.module;


import javax.inject.Singleton;

import android.content.Context;

import com.hedgehog.mousai.dal.MusicFilesDao;

import dagger.Module;
import dagger.Provides;


@Module
public class DalModule {

    @Provides
    @Singleton
    MusicFilesDao provideMusicFileDao(Context context) {
        return new MusicFilesDao(context);
    }
}
