package com.hedgehog.mousai.util;


/**
 * Class containing static util methods that are being use across the application. These methods are standalone and have
 * nothing in common with the application.
 */
public class StringUtils {

    public static String formatTime(long milliseconds) {
        StringBuilder stringBuilder = new StringBuilder();
        long seconds = (milliseconds / 1000) % 60;
        long minutes = ((milliseconds / (1000 * 60)) % 60);
        stringBuilder.append(minutes);
        stringBuilder.append(":");
        if (seconds < 10) {
            stringBuilder.append("0");
        }
        stringBuilder.append(seconds);

        return stringBuilder.toString();
    }
}
