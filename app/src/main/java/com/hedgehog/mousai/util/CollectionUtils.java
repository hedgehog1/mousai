package com.hedgehog.mousai.util;

import java.util.Collection;

/**
 * Class containing util functions for managing collections
 */
public class CollectionUtils {

    public static boolean isNullOrEmpty(Collection collection) {
        if (collection == null || collection.isEmpty()) {
            return true;
        }

        return false;
    }
}
