package com.hedgehog.mousai.util;


import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;


/**
 * Class with utilities for using the android system services.
 */
public class SystemServicesUtils {

    /**
     * Check whether a service is currently running.
     * 
     * @param appContext
     *            the {@link Context context} to get the system service from. Preferably the application context and not
     *            some component.
     * @param serviceClass
     *            the class of the service that is checked
     * @param <T>
     *            the class of the service that is checked
     * @return whether the service is running or not
     */
    public static <T extends Service> boolean isServiceRunning(Context appContext, Class<T> serviceClass) {
        ActivityManager manager = (ActivityManager) appContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
