package com.hedgehog.mousai.util;


import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;


/**
 * Class with utilities for using the android system services.
 */
public class SystemServicesUtils {

    /**
     * Check whether a service is currently running.
     * 
     * @param appContext
     *            the {@link Context context} to get the system service from. Preferably the application context and not
     *            some component.
     * @param serviceClass
     *            the class of the service that is checked
     * @param <T>
     *            the class of the service that is checked
     * @return whether the service is running or not
     */
    public static <T extends Service> boolean isServiceRunning(Context appContext, Class<T> serviceClass) {
        ActivityManager manager = (ActivityManager) appContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public static void setStatusBarColor(Activity activity, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(color);
        }
    }

}
