package com.hedgehog.mousai.util;


import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import com.hedgehog.mousai.R;
import com.hedgehog.mousai.controller.dialog.PermissionDeniedDialog;

import static android.support.v4.app.ActivityCompat.shouldShowRequestPermissionRationale;


public class PermissionsHelper {

    private final int REQUEST_CODE_PERMISSION_STORAGE = 5312;



    public interface OnStoragePermissionActionListener {

        void onStoragePermissionGranted();

        void onStoragePermissionDenied();
    }



    private Activity                          mActivity;
    private OnStoragePermissionActionListener mOnStoragePermissionActionListener;


    public PermissionsHelper(Activity activity) {
        mActivity = activity;
    }


    public void actionOnStoragePermission(OnStoragePermissionActionListener listener) {
        if (listener != null) {
            if (mOnStoragePermissionActionListener != null) {
                // if the permission request is in progress just replace the listener
                mOnStoragePermissionActionListener = listener;
                return;
            } else {
                mOnStoragePermissionActionListener = listener;
            }
            //Check if we have the Camera Permission
            if (!hasReadWriteStoragePermissionGranted()) {
                // Check if we need to show an explanation dialog about why we need the camera permission
                if (shouldShowRequestStoragePermissionRationale()) {
                    openStoragePermissionExplanationDialog();
                } else {
                    requestStoragePermission();
                }
            } else {
                mOnStoragePermissionActionListener.onStoragePermissionGranted();
                mOnStoragePermissionActionListener = null;
            }
        }
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
            @NonNull int[] grantResults) {
        switch (requestCode) {
        case REQUEST_CODE_PERMISSION_STORAGE:
            onPermissionRequestStorage(permissions, grantResults);
            break;
        default: {
            // Nothing to do
        }
        }
    }


    private void openStoragePermissionExplanationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(mActivity.getString(R.string.request_storage_permission_dialog_title));
        builder.setMessage(mActivity.getString(R.string.request_storage_permission_dialog_message));
        builder.setPositiveButton(mActivity.getString(R.string.dialog_button_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestStoragePermission();
                    }
                }).show();
    }


    private void requestStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.requestPermissions(
                    new String[] { Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE

                    }, REQUEST_CODE_PERMISSION_STORAGE);
        }

    }


    private boolean hasReadWriteStoragePermissionGranted() {
        boolean hasReadExternalStoragePermission = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            hasReadExternalStoragePermission =
                    ContextCompat.checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED;
        }

        return hasReadExternalStoragePermission || (
                ContextCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED);
    }


    private boolean shouldShowRequestStoragePermissionRationale() {
        boolean checkReadExternalStorage = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            checkReadExternalStorage = shouldShowRequestPermissionRationale(mActivity,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        return checkReadExternalStorage || shouldShowRequestPermissionRationale(mActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }


    private boolean shouldShowRequestLocationPermissionRationale() {
        return shouldShowRequestPermissionRationale(mActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                || shouldShowRequestPermissionRationale(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION);
    }


    private void onPermissionRequestStorage(String permissions[], int[] grantResults) {
        if (mOnStoragePermissionActionListener == null) {
            return;
        }
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0) {
            boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[0]);
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mOnStoragePermissionActionListener.onStoragePermissionGranted();
            } else {
                if (!showRationale) {
                    // The permission is always denied
                    new PermissionDeniedDialog(mActivity)
                            .show(mActivity.getString(R.string.storage_permission_denied_dialog_title),
                                    mActivity.getString(R.string.storage_permission_denied_dialog_message));
                }
                mOnStoragePermissionActionListener.onStoragePermissionDenied();
            }
        }
        mOnStoragePermissionActionListener = null;
    }
}