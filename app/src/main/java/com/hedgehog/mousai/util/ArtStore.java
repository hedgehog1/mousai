package com.hedgehog.mousai.util;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;

import com.android.util.ImageCache;
import com.android.util.ImageWorker;
import com.hedgehog.mousai.model.music.MusicCollection;
import com.hedgehog.mousai.model.music.MusicFile;
import com.hedgehog.mousai.view.ImageLoader;


/**
 * Class that handles retrieving of art from a {@link MusicFile music files}. Supports lazy loading of art through
 * {@link FileArtLoader single file loaders} or {@link CollectionArtLoader collection loaders} and also simple art
 * retrieving from its static methods. Can provide images in both {@link Format#FULL full size} or
 * {@link Format#THUMBNAIL in thumbnails}. The processed images are cached for faster access.
 */
public class ArtStore {

    private static final int DEFAULT_THUMBNAIL_SIZE = 150;

    private static final String DEFAULT_THUMBNAIL_SUBFOLDER = "/thumbnail";
    private ImageWorker mImageWorker;
    private ImageWorker mThumbnailWorker;
    private Bitmap mPlaceholder;
    private FragmentActivity mActivity;
    private int mThumbnailSize;

    public ArtStore(FragmentActivity activity, int placeholderResId, String cacheFolder, float cacheMemPercent) {
        mActivity = activity;
        mPlaceholder = BitmapFactory.decodeResource(mActivity.getResources(), placeholderResId);
        mThumbnailSize = DEFAULT_THUMBNAIL_SIZE;
        initFullImageWorker(cacheFolder, cacheMemPercent / 2);
        initThumbnailImageWorker(cacheFolder + DEFAULT_THUMBNAIL_SUBFOLDER, cacheMemPercent / 2);
    }


    /**
     * Retrieve full size album art for the specified {@link MusicFile music file}.
     *
     * @param file
     *            the {@link MusicFile file} for which the art should be retrieved
     * @return a {@link Bitmap} with the album art or <code>null</code> if the music file does not have an embedded
     *         image
     */
    public static Bitmap getAlbumArt(MusicFile file) {
        if (file != null) {
            MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
            metaRetriever.setDataSource(file.getPath());
            byte[] art = metaRetriever.getEmbeddedPicture();
            if (art != null) {
                return BitmapFactory.decodeByteArray(art, 0, art.length);
            }
        }
        return null;
    }

    
    /**
     * Retrieve thumbnail size album art for the specified {@link MusicFile music file}.
     *
     * @param file
     *            the {@link MusicFile file} for which the art should be retrieved
     * @return a {@link Bitmap} with the album art as thumbnail or <code>null</code> if the music file does not have an
     *         embedded image
     */
    public static Bitmap getAlbumArtThumbnail(MusicFile file, int thumbnailSize) {
        Bitmap albumArt = getAlbumArt(file);
        if (albumArt != null) {
            return ThumbnailUtils.extractThumbnail(getAlbumArt(file), thumbnailSize, thumbnailSize);
        } else {
            return null;
        }
    }


    /**
     * Retrieve a {@link CollectionArtLoader collection art loader} with the desired {@link Format format}.
     *
     * @param collectionType
     *            The collection type for which an art should be provided
     * @param format
     *            the {@link Format format} of the art
     * @return the appropriate {@link CollectionArtLoader}
     */
    public CollectionArtLoader getCollectionArtLoader(MusicCollection.Type collectionType, Format format) {
        return defaultCollectionLoader(format);
    }

    /**
     * Retrieve a {@link FileArtLoader file art loader} with the desired {@link Format format}.
     *
     * @param format
     *            the {@link Format format} of the art
     * @return the appropriate {@link FileArtLoader}
     */
    public FileArtLoader getFileArtLoader(Format format) {
        return defaultLoader(format);
    }

    
    /**
     * Load album art for the specified {@link MusicFile music file} into the {@link ImageView image view} with the
     * specified {@link Format format}.
     *
     * @param file
     *            the {@link MusicFile file} for which the art should be loaded
     * @param imageView
     *            the {@link ImageView image view} where the art should be loaded
     * @param format
     *            the format to use when loading the art
     */
    public void loadArt(MusicFile file, ImageView imageView, Format format) {
        switch (format) {
        case THUMBNAIL:
            mThumbnailWorker.loadImage(file, imageView);
            break;
        case FULL:
        default:
            mImageWorker.loadImage(file, imageView);
        }
    }

    private FileLoader defaultLoader(Format format) {
        switch (format) {
        case THUMBNAIL:
            return new FileLoader(mThumbnailWorker);
        case FULL:
        default:
            return new FileLoader(mImageWorker);
        }
    }

    private CollectionArtLoader defaultCollectionLoader(Format format) {
        switch (format) {
        case THUMBNAIL:
            return new CollectionLoader(mThumbnailWorker);
        case FULL:
        default:
            return new CollectionLoader(mImageWorker);
        }
    }

    private void initFullImageWorker(String cacheFolder, float cacheMemPercent) {
        mImageWorker = new ImageWorker(mActivity) {

            @Override
            protected Bitmap processBitmap(Object data) {
                if (data instanceof MusicFile) {
                    MusicFile file = (MusicFile) data;

                    try {
                        Bitmap art = getAlbumArt(file);
                        return art != null ? art : mPlaceholder;
                    } catch (Exception e) {
                        // if something fails use the default image
                    }
                }
                return mPlaceholder;
            }
        };

        ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(mActivity, cacheFolder);

        cacheParams.setMemCacheSizePercent(cacheMemPercent);
        mImageWorker.addImageCache(mActivity.getSupportFragmentManager(), cacheParams);
    }

    private void initThumbnailImageWorker(String cacheFolder, float cacheMemPercent) {
        mThumbnailWorker = new ImageWorker(mActivity) {

            private Bitmap mDefaultThumbnail = ThumbnailUtils.extractThumbnail(mPlaceholder, mThumbnailSize,
                    mThumbnailSize);


            @Override
            protected Bitmap processBitmap(Object data) {
                if (data instanceof MusicFile) {
                    MusicFile file = (MusicFile) data;

                    try {
                        Bitmap art = getAlbumArtThumbnail(file, mThumbnailSize);
                        return art != null ? art : mDefaultThumbnail;
                    } catch (Exception e) {
                        // if something fails use the default image
                    }
                }
                return mDefaultThumbnail;
            }
        };

        ImageCache.ImageCacheParams cacheThumbParams = new ImageCache.ImageCacheParams(mActivity, cacheFolder);
        cacheThumbParams.setMemCacheSizePercent(cacheMemPercent);
        mThumbnailWorker.addImageCache(mActivity.getSupportFragmentManager(), cacheThumbParams);
    }

    /**
     * The format an AlbumArt can take.
     */
    public enum Format {
        THUMBNAIL, FULL
    }

    /**
     * Loader for a single {@link MusicFile music file} to an {@link ImageView image view}.
     */
    public interface FileArtLoader extends ImageLoader<MusicFile> {
    }

    /**
     * Loader for a {@link MusicCollection music collection} to an {@link ImageView image view}.
     */
    public interface CollectionArtLoader extends ImageLoader<MusicCollection> {
    }

    private class FileLoader implements FileArtLoader {

        private ImageWorker mWorker;


        public FileLoader(ImageWorker worker) {
            mWorker = worker;
        }


        @Override
        public void load(MusicFile file, ImageView view) {
            mWorker.loadImage(file, view);
        }

    }

    private class CollectionLoader implements CollectionArtLoader {

        private ImageWorker mWorker;


        public CollectionLoader(ImageWorker worker) {
            mWorker = worker;
        }


        @Override
        public void load(MusicCollection collection, ImageView view) {
            if (!collection.isEmpty()) {
                mWorker.loadImage(collection.get(0), view);
            }
        }
    }

}
