package com.hedgehog.mousai.util.service;


import android.app.IntentService;
import android.content.Intent;

import com.hedgehog.mousai.constant.IntentAction;
import com.hedgehog.mousai.constant.IntentExtra;

import de.greenrobot.event.EventBus;


/**
 * Service with intent filter for intent action {@link IntentAction#INTENT_ACTION_PROXY_TO_EVENT_BUS} and expects intent
 * extra {@link IntentExtra#INTENT_EXTRA_EVENT_OBJECT}. The extra event object is then posted to the event bus within
 * the main process of the application.
 */
public class EventBusProxyService extends IntentService {

    public EventBusProxyService() {
        super("EventBusProxyService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent.getAction().equals(IntentAction.INTENT_ACTION_PROXY_TO_EVENT_BUS)
                && intent.hasExtra(IntentExtra.INTENT_EXTRA_EVENT_OBJECT)) {
            EventBus.getDefault().post(intent.getSerializableExtra(IntentExtra.INTENT_EXTRA_EVENT_OBJECT));
        }
    }
}
