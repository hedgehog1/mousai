package com.hedgehog.mousai.util;


import android.graphics.Color;


public class ColorUtils {

    public static int setAlpha(int rgbColor, float alphaFactor) {
        int alpha = Math.round(Color.alpha(rgbColor) * alphaFactor);
        int red = Color.red(rgbColor);
        int green = Color.green(rgbColor);
        int blue = Color.blue(rgbColor);
        return Color.argb(alpha, red, green, blue);
    }
}
