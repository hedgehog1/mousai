package com.hedgehog.mousai.theme;


import android.graphics.Bitmap;
import android.support.v7.graphics.Palette;

import com.hedgehog.mousai.event.controller.service.MusicPlayerServiceEvent;
import com.hedgehog.mousai.event.theme.ThemeEvent;
import com.hedgehog.mousai.util.ArtStore;

import de.greenrobot.event.EventBus;


public class SongArtThemeProvider implements ThemeProvider {

    private EventBus mEventBus;
    private Theme    mTheme;
    private Theme    mDefaultTheme;


    public SongArtThemeProvider(EventBus eventBus, Theme defaultTheme) {
        mEventBus = eventBus;
        if (mEventBus != null) {
            mEventBus.register(this);
        }
        mTheme = mDefaultTheme = defaultTheme;
    }


    @Override
    public Theme getTheme() {
        return mTheme;
    }


    public void release() {
        if (mEventBus != null) {
            mEventBus.unregister(this);
        }
    }


    public void onEventMainThread(MusicPlayerServiceEvent.MusicFileChanged event) {
        Bitmap albumArt = ArtStore.getAlbumArt(event.musicFile);
        if (albumArt != null) {
            Palette.from(albumArt).maximumColorCount(10).resizeBitmapSize(50)
                    .generate(new Palette.PaletteAsyncListener() {

                        @Override
                        public void onGenerated(Palette palette) {
                            Palette.Swatch primarySwatch = palette.getVibrantSwatch();

                            if (primarySwatch == null) {
                                primarySwatch = palette.getSwatches().get(0);
                            }
                            int primaryColor = primarySwatch.getRgb();
                            int primaryTextColor = primarySwatch.getTitleTextColor();

                            Palette.Swatch secondarySwatch = palette.getDarkVibrantSwatch();

                            if (secondarySwatch == null) {
                                secondarySwatch = palette.getSwatches().get(1);
                            }
                            int secondaryColor = secondarySwatch.getRgb();
                            int secondaryTextColor = secondarySwatch.getTitleTextColor();

                            mTheme = new Theme(primaryColor, primaryTextColor, secondaryColor, secondaryTextColor);
                            if (mEventBus != null) {
                                mEventBus.post(new ThemeEvent.ThemeChanged(mTheme));
                            }
                        }
                    });
        } else {
            mTheme = mDefaultTheme;
            if (mEventBus != null) {
                mEventBus.post(new ThemeEvent.ThemeChanged(mTheme));
            }
        }
    }

}
