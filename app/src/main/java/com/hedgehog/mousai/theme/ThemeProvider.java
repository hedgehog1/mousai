package com.hedgehog.mousai.theme;


public interface ThemeProvider {

    Theme getTheme();
}
