package com.hedgehog.mousai.theme;


public class Theme {

    private int mPrimaryColor;
    private int mPrimaryTextColor;
    private int mSecondaryColor;
    private int mSecondaryTextColor;


    public Theme(int primaryColor, int primaryTextColor, int secondaryColor, int secondaryTextColor) {
        mPrimaryColor = primaryColor;
        mPrimaryTextColor = primaryTextColor;
        mSecondaryColor = secondaryColor;
        mSecondaryTextColor = secondaryTextColor;
    }


    public int getPrimaryColor() {
        return mPrimaryColor;
    }


    public int getPrimaryTextColor() {
        return mPrimaryTextColor;
    }


    public int getSecondaryColor() {
        return mSecondaryColor;
    }


    public int getSecondaryTextColor() {
        return mSecondaryTextColor;
    }

}
